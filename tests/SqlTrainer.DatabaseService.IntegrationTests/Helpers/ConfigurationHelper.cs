using Microsoft.Extensions.Configuration;

namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

public sealed class MssqlConfigurationHelper : ConfigurationHelper
{
    protected override string ServerConfigKey => "MSSQL_SERVER";
    protected override string DatabaseConfigKey => "MSSQL_DATABASE";
    protected override string UserConfigKey => "MSSQL_USER";
    protected override string PasswordConfigKey => "MSSQL_PASSWORD";
    protected override string RunTestsKey => "RUN_MSSQL_TESTS";
    protected override string BaseConnectionStringKey => "BaseMssql";
}

public sealed class PostgresConfigurationHelper : ConfigurationHelper
{
    protected override string ServerConfigKey => "POSTGRES_SERVER";
    protected override string PortConfigKey => "POSTGRES_PORT";
    protected override string DatabaseConfigKey => "POSTGRES_DATABASE";
    protected override string UserConfigKey => "POSTGRES_USER";
    protected override string PasswordConfigKey => "POSTGRES_PASSWORD";
    protected override string RunTestsKey => "RUN_POSTGRES_TESTS";
    protected override string BaseConnectionStringKey => "BasePostgres";
}

public abstract class ConfigurationHelper
{
    private static IConfiguration? configuration;
    public IConfiguration Configuration => configuration ??= new ConfigurationBuilder()
        .AddJsonFile("appsettings.json")
        .AddEnvironmentVariables()
        .Build();
    
    protected virtual string ServerConfigKey => "SERVER";
    protected virtual string PortConfigKey => "PORT";
    protected virtual string UserConfigKey => "USER";
    protected virtual string PasswordConfigKey => "PASSWORD";
    protected virtual string DatabaseConfigKey => "DATABASE";
    protected virtual string RunTestsKey => "RUN_TESTS";
    protected abstract string BaseConnectionStringKey { get; }

    public string GetServer() => GetConfigValue<string>(ServerConfigKey);
    public string GetPort() => GetConfigValue<int>(PortConfigKey);
    public string GetUser() => GetConfigValue<string>(UserConfigKey);
    public string GetPassword() => GetConfigValue<string>(PasswordConfigKey);
    public string GetDatabase() => GetConfigValue<string>(DatabaseConfigKey);
    public string GetBaseConnectionString() => Configuration.GetConnectionString(BaseConnectionStringKey)!;
    
    public bool RunTests => Configuration.GetValue<bool>(RunTestsKey);
    
    private string GetConfigValue<TValue>(string key, string defaultValue = "") =>
        Configuration.GetValue<TValue>(key)?.ToString() ?? defaultValue;
}
