using Microsoft.Data.SqlClient;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Factories;

namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

public abstract class InitializationHelper
{
    public const string Mssql = "MSSQL";
    public const string Postgres = "POSTGRESQL";

    private static readonly DatabaseConfiguration<MssqlConfigurationHelper> MssqlConfiguration = new(new MssqlConfigurationHelper());
    private static readonly DatabaseConfiguration<PostgresConfigurationHelper> PostgresConfiguration = new(new PostgresConfigurationHelper());

    public static DatabasesCreationNotes DatabasesCreationNotes { get; } = new();

    public static string GetConnectionString(string language, string? databaseName = null)
    {
        return language.ToUpper() switch
        {
            Mssql => MssqlConfiguration.BuildConnectionString(databaseName),
            Postgres => PostgresConfiguration.BuildConnectionString(databaseName),
            _ => throw new ArgumentOutOfRangeException(nameof(language), language, "Language not supported")
        };
    }

    public static bool ShouldRunTests(string language)
    {
        return language.ToUpper() switch
        {
            Mssql => MssqlConfiguration.RunTests,
            Postgres => PostgresConfiguration.RunTests,
            _ => throw new ArgumentOutOfRangeException(nameof(language), language, "Language not supported")
        };
    }

    public static IDatabaseRepository CreateDatabaseRepository(string language, string databaseName, IRepositoryFactory? repositoryFactory = null)
    {
        var connectionString = GetConnectionString(language, databaseName);
        repositoryFactory ??= new RepositoryFactory();
        return repositoryFactory.CreateDatabaseRepository(connectionString, language);
    }
    
    protected static async Task CreateDatabaseAsync(string language, string name)
    {
        if (!ShouldRunTests(language) || DatabasesCreationNotes.DoesDatabaseExist(language, name))
            return;
        
        var connectionString = GetConnectionString(language);
        var repositoryFactory = new RepositoryFactory();
        var databaseRepository = repositoryFactory.CreateDatabaseRepository(connectionString, language);
        await databaseRepository.CreateAsync(name);
        DatabasesCreationNotes.AddCreationNote(language, name);
    }

    protected static async Task DropDatabaseAsync(string language, string name)
    {
        if (!ShouldRunTests(language) || !DatabasesCreationNotes.DoesDatabaseExist(language, name))
            return;
        
        var connectionString = GetConnectionString(language);
        if (language.Equals(Mssql))
            await SetMultiUserAsync(connectionString, name);

        var repositoryFactory = new RepositoryFactory();
        var databaseRepository = repositoryFactory.CreateDatabaseRepository(connectionString, language);
        await databaseRepository.DropAsync(name);
        
        DatabasesCreationNotes.AddDropNote(language, name);
    }

    private static async Task SetMultiUserAsync(string connectionString, string name)
    {
        await using var connection = new SqlConnection(connectionString);
        await connection.OpenAsync();
        await using var command = new SqlCommand($"ALTER DATABASE [{name}] SET MULTI_USER WITH ROLLBACK IMMEDIATE", connection);
        await command.ExecuteNonQueryAsync();
    } 
}