using SqlTrainer.DatabaseService.IntegrationTests.Databases;
using SqlTrainer.DatabaseService.IntegrationTests.Tables;

namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

public sealed class DatabaseHelper : IAsyncLifetime
{
    public DatabaseServiceInitializationHelper DatabasesHelper { get; } = new();
    public TableServiceInitializationHelper TablesHelper { get; } = new();
    
    public async Task InitializeAsync()
    {
        await DatabaseServiceInitializationHelper.InitializeAsync();
        await TableServiceInitializationHelper.InitializeAsync();
    }

    public async Task DisposeAsync()
    {
        await DatabaseServiceInitializationHelper.CleanUpAsync();
        await TableServiceInitializationHelper.CleanUpAsync();
    }
}