namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

[CollectionDefinition(Name)]
public class DatabaseDefinition : ICollectionFixture<DatabaseHelper>
{
    public const string Name = "Initialization";
}