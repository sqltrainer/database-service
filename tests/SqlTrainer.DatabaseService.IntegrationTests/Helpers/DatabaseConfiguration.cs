using Microsoft.Extensions.Configuration;

namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

public sealed class DatabaseConfiguration<TConfigurationHelper> where TConfigurationHelper : ConfigurationHelper
{
    private readonly string? server;
    private readonly string? port;
    private readonly string? database;
    private readonly string? user;
    private readonly string? password;

    private readonly string baseConnectionString;
    
    public bool RunTests { get; }
    
    public DatabaseConfiguration(TConfigurationHelper configurationHelper)
    {
        server = configurationHelper.GetServer();
        port = configurationHelper.GetPort();
        database = configurationHelper.GetDatabase();
        user = configurationHelper.GetUser();
        password = configurationHelper.GetPassword();

        baseConnectionString = configurationHelper.GetBaseConnectionString();
        
        RunTests = configurationHelper.RunTests;
    }

    public string BuildConnectionString(string? customDatabaseName = null) =>
        BuildConnectionString(baseConnectionString,
            server ?? string.Empty,
            port ?? string.Empty,
            customDatabaseName ?? database ?? string.Empty,
            user ?? string.Empty,
            password ?? string.Empty);

    private static string BuildConnectionString(string baseConnectionString, string server, string port, string database, string user, string password) =>
        baseConnectionString
            .Replace("{Server}", server)
            .Replace("{Port}", port)
            .Replace("{Database}", database)
            .Replace("{User}", user)
            .Replace("{Password}", password);
}