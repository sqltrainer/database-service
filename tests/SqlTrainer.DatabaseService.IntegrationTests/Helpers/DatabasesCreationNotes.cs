namespace SqlTrainer.DatabaseService.IntegrationTests.Helpers;

public sealed class DatabasesCreationNotes
{
    private readonly IDictionary<string, bool> databasesCreated = new Dictionary<string, bool>();

    public void AddCreationNote(string language, string name)
    {
        AddDatabaseNote(language, name, true);
    }
    
    public void AddDropNote(string language, string name)
    {
        AddDatabaseNote(language, name, false);
    }
    
    public bool DoesDatabaseExist(string language, string name)
    {
        var key = GetDatabasesDictionaryKey(language, name);
        return databasesCreated.ContainsKey(key) && databasesCreated[key];
    }
    
    private void AddDatabaseNote(string language, string name, bool value)
    {
        var key = GetDatabasesDictionaryKey(language, name);
        if (databasesCreated.ContainsKey(key))
            databasesCreated[key] = value;
        else
            databasesCreated.Add(key, value);
    }
    
    private static string GetDatabasesDictionaryKey(string language, string name) => language + name;
}