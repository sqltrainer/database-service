using SqlTrainer.DatabaseService.Factories;
using SqlTrainer.DatabaseService.IntegrationTests.Helpers;

namespace SqlTrainer.DatabaseService.IntegrationTests.Tables;

public sealed class TableServiceInitializationHelper : InitializationHelper
{
    public const string DatabaseTestName = "DatabaseTest";
    
    public const string AlterTableName = "AlterTestTable";
    public const string RenameTableName = "RenameTestTable";
    public const string DropTableName = "DropTestTable";
    
    public static async Task InitializeAsync()
    {
        await CreateDatabaseWithAlterAndDropTablesAsync(Mssql, "CREATE TABLE [@Table] ([Id] int, [Name] varchar(50))");
        await CreateDatabaseWithAlterAndDropTablesAsync(Postgres, "CREATE TABLE \"@Table\" (\"Id\" int, \"Name\" varchar(50))");
    }
    
    public static async Task CleanUpAsync()
    {
        await DropDatabaseAsync(Mssql, DatabaseTestName);
        await DropDatabaseAsync(Postgres, DatabaseTestName);
    }

    private static async Task CreateDatabaseWithAlterAndDropTablesAsync(string language, string createTableQueryPattern)
    {
        if (!ShouldRunTests(language) || DatabasesCreationNotes.DoesDatabaseExist(language, DatabaseTestName))
            return;
        
        await CreateDatabaseAsync(language, DatabaseTestName);
        
        var repositoryFactory = new RepositoryFactory();
        var connectionString = GetConnectionString(language, DatabaseTestName);
        var unitOfWork = repositoryFactory.CreateUnitOfWork(connectionString, language);
        var databaseRepository = CreateDatabaseRepository(language, DatabaseTestName, repositoryFactory);
        await databaseRepository.ExecuteAsync(createTableQueryPattern.Replace("@Table", AlterTableName));
        await databaseRepository.ExecuteAsync(createTableQueryPattern.Replace("@Table", DropTableName));
        await databaseRepository.ExecuteAsync(createTableQueryPattern.Replace("@Table", RenameTableName));
        unitOfWork.SaveChanges();
    }
}