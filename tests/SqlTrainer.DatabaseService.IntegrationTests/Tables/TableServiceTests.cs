using Microsoft.Data.SqlClient;
using Results;
using SqlTrainer.DatabaseService.Application.Tables.Alter;
using SqlTrainer.DatabaseService.Application.Tables.Create;
using SqlTrainer.DatabaseService.Application.Tables.Drop;
using SqlTrainer.DatabaseService.Application.Tables.Rename;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;
using SqlTrainer.DatabaseService.IntegrationTests.Databases;
using SqlTrainer.DatabaseService.IntegrationTests.Helpers;

namespace SqlTrainer.DatabaseService.IntegrationTests.Tables;

[Collection(DatabaseDefinition.Name)]
public class TableServiceTests
{
    private const string Mssql = InitializationHelper.Mssql;
    private const string Postgres = InitializationHelper.Postgres;
    
    private const string IdAttributeName = "Id";
    private const string IdAttributeType = "int";
    private const string NameAttributeName = "Name";
    private const string NameAttributeType = "varchar";
    private const int NameAttributeSize = 50;
    
    private const string DatabaseName = TableServiceInitializationHelper.DatabaseTestName;

    public sealed class CreateTests : TableServiceTests
    {
        private const string TableName = "CreateTestTable";

        public static IEnumerable<object[]> CreateDatabaseTestData()
        {
            var testData = new List<object[]>();

            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseName, TableName, IdAttributeName, NameAttributeName, 0]);
                testData.Add([Mssql, DatabaseName, "Invalid name", IdAttributeName, NameAttributeName, 1]);
                testData.Add([Mssql, DatabaseName, TableName, "Invalid name", NameAttributeName, 1]);
                testData.Add([Mssql, DatabaseName, TableName, IdAttributeName, "Invalid name", 1]);
                testData.Add([Mssql, DatabaseName, "Invalid name", "Invalid name", NameAttributeName, 2]);
                testData.Add([Mssql, DatabaseName, "Invalid name", IdAttributeName, "Invalid name", 2]);
                testData.Add([Mssql, DatabaseName, TableName, "Invalid name", "Invalid name", 2]);
                testData.Add([Mssql, DatabaseName, "Invalid name", "Invalid name", "Invalid name", 3]);
            }

            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseName, TableName, IdAttributeName, NameAttributeName, 0]);
                testData.Add([Postgres, DatabaseName, "Invalid name", IdAttributeName, NameAttributeName, 1]);
                testData.Add([Postgres, DatabaseName, TableName, "Invalid name", NameAttributeName, 1]);
                testData.Add([Postgres, DatabaseName, TableName, IdAttributeName, "Invalid name", 1]);
                testData.Add([Postgres, DatabaseName, "Invalid name", "Invalid name", NameAttributeName, 2]);
                testData.Add([Postgres, DatabaseName, "Invalid name", IdAttributeName, "Invalid name", 2]);
                testData.Add([Postgres, DatabaseName, TableName, "Invalid name", "Invalid name", 2]);
                testData.Add([Postgres, DatabaseName, "Invalid name", "Invalid name", "Invalid name", 3]);
            }

            return testData;
        }
        
        [Theory]
        [MemberData(nameof(CreateDatabaseTestData))]
        public async Task Create_ReturnResponse(string language, string databaseName, string tableName, string idName, string nameName, int errorsCount)
        {
            // Arrange
            var connectionString = InitializationHelper.GetConnectionString(language, databaseName);
            TableAttributeRequest[] attributes =
            [
                new TableAttributeRequest(idName, IdAttributeType, null, null, true, true, false, null, null, 1),
                new TableAttributeRequest(nameName, NameAttributeType, null, NameAttributeSize, false, true, true, null, null, 2)
            ];

            var request = new CreateTableRequest(language, connectionString, tableName, attributes);
            
            // Act
            var sut = DatabaseServiceInitializationHelper.CreateCreateTableRequestHandler();
            var response = await sut.HandleAsync(request);
            
            // Assert
            response.Should().BeOfType<Result>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var scriptResult = await databaseRepository.ExecuteAsync($"SELECT * FROM \"{tableName}\"");
                scriptResult.Should().NotBeNull();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public sealed class DropTests : TableServiceTests
    {
        private const string TableName = TableServiceInitializationHelper.DropTableName;

        public static IEnumerable<object[]> GetDropTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseName, TableName, 0]);
                testData.Add([Mssql, DatabaseName, "Invalid name", 1]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseName, TableName, 0]);
                testData.Add([Postgres, DatabaseName, "Invalid name", 1]);
            }
            
            return testData;
        }
        
        [Theory]
        [MemberData(nameof(GetDropTestData))]
        public async Task Drop_ReturnResponse(string language, string databaseName, string tableName, int errorsCount)
        {
            // Arrange
            var connectionString = InitializationHelper.GetConnectionString(language, databaseName);
            var request = new DropTableRequest(language, connectionString, tableName);
            
            // Act
            var sut = DatabaseServiceInitializationHelper.CreateDropTableRequestHandler();
            var response = await sut.HandleAsync(request);
            
            // Assert
            response.Should().BeOfType<Result>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var act = async () => await databaseRepository.ExecuteAsync($"SELECT * FROM \"{tableName}\"");
                switch (language)
                {
                    case InitializationHelper.Mssql:
                        await act.Should().ThrowAsync<SqlException>();
                        break;
                    case InitializationHelper.Postgres:
                        await act.Should().ThrowAsync<Npgsql.PostgresException>();
                        break;
                }
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public sealed class AlterTests : TableServiceTests
    {
        private const string TableName = TableServiceInitializationHelper.AlterTableName;

        public static IEnumerable<object[]> GetAlterTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseName, TableName, IdAttributeName, NameAttributeName, 0]);
                testData.Add([Mssql, DatabaseName, "Invalid name", IdAttributeName, NameAttributeName, 1]);
                testData.Add([Mssql, DatabaseName, TableName, "Invalid name", NameAttributeName, 1]);
                testData.Add([Mssql, DatabaseName, TableName, IdAttributeName, "Invalid name", 1]);
                testData.Add([Mssql, DatabaseName, "Invalid name", "Invalid name", NameAttributeName, 2]);
                testData.Add([Mssql, DatabaseName, "Invalid name", IdAttributeName, "Invalid name", 2]);
                testData.Add([Mssql, DatabaseName, TableName, "Invalid name", "Invalid name", 2]);
                testData.Add([Mssql, DatabaseName, "Invalid name", "Invalid name", "Invalid name", 3]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseName, TableName, IdAttributeName, NameAttributeName, 0]);
                testData.Add([Postgres, DatabaseName, "Invalid name", IdAttributeName, NameAttributeName, 1]);
                testData.Add([Postgres, DatabaseName, TableName, "Invalid name", NameAttributeName, 1]);
                testData.Add([Postgres, DatabaseName, TableName, IdAttributeName, "Invalid name", 1]);
                testData.Add([Postgres, DatabaseName, "Invalid name", "Invalid name", NameAttributeName, 2]);
                testData.Add([Postgres, DatabaseName, "Invalid name", IdAttributeName, "Invalid name", 2]);
                testData.Add([Postgres, DatabaseName, TableName, "Invalid name", "Invalid name", 2]);
                testData.Add([Postgres, DatabaseName, "Invalid name", "Invalid name", "Invalid name", 3]);
            }
            
            return testData;
        }
        
        [Theory]
        [MemberData(nameof(GetAlterTestData))]
        public async Task Alter_ReturnResponse(string language, string databaseName, string tableName, string idName, string nameName, int errorsCount)
        {
            // Arrange
            var connectionString = InitializationHelper.GetConnectionString(language, databaseName);

            AlterTableAttributeVersions[] attributes = 
            [
                new AlterTableAttributeVersions(
                    new TableAttributeRequest(idName, IdAttributeType, null, null, false, false, false, null, null, 1),
                    new TableAttributeRequest(IdAttributeName, IdAttributeType, null, null, true, true, false, null, null, 1)
                ),
                new AlterTableAttributeVersions(
                    new TableAttributeRequest(nameName, NameAttributeType, null, NameAttributeSize, false, false, false, null, null, 2),
                    new TableAttributeRequest(NameAttributeName, NameAttributeType, null, NameAttributeSize, false, true, true, null, null, 2)
                ),
                new AlterTableAttributeVersions(
                    null,
                    new TableAttributeRequest("Description", "varchar", null, 100, false, false, false, null, null, 3)
                )
            ];

            var request = new AlterTableRequest(language, connectionString, tableName, attributes);
            
            // Act
            var sut = DatabaseServiceInitializationHelper.CreateAlterTableRequestHandler();
            var response = await sut.HandleAsync(request);
            
            // Assert
            response.Should().BeOfType<Result>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var scriptResult = await databaseRepository.ExecuteAsync($"SELECT * FROM \"{tableName}\"");
                scriptResult.Should().NotBeNull();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    } 
    
    public sealed class RenameTests : TableServiceTests
    {
        private const string TableName = TableServiceInitializationHelper.RenameTableName;

        public static IEnumerable<object[]> GetRenameTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseName, TableName, 0]);
                testData.Add([Mssql, DatabaseName, "Invalid name", 1]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseName, TableName, 0]);
                testData.Add([Postgres, DatabaseName, "Invalid name", 1]);
            }
            
            return testData;
        }
        
        [Theory]
        [MemberData(nameof(GetRenameTestData))]
        public async Task Alter_ReturnResponse(string language, string databaseName, string tableName, int errorsCount)
        {
            // Arrange
            var alteredName = TableName + "Updated";
            var connectionString = InitializationHelper.GetConnectionString(language, databaseName);
            var request = new RenameTableRequest(language, connectionString, tableName, alteredName);
            
            // Act
            var sut = DatabaseServiceInitializationHelper.CreateRenameTableRequestHandler();
            var response = await sut.HandleAsync(request);
            
            // Assert
            response.Should().BeOfType<Result>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var scriptResult = await databaseRepository.ExecuteAsync($"SELECT * FROM \"{alteredName}\"");
                scriptResult.Should().NotBeNull();
                var act = async () => await databaseRepository.ExecuteAsync($"SELECT * FROM \"{tableName}\"");
                switch (language)
                {
                    case InitializationHelper.Mssql:
                        await act.Should().ThrowAsync<SqlException>();
                        break;
                    case InitializationHelper.Postgres:
                        await act.Should().ThrowAsync<Npgsql.PostgresException>();
                        break;
                }
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }
}