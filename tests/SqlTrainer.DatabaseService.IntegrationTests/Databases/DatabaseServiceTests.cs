using Microsoft.Data.SqlClient;
using Results;
using SqlTrainer.DatabaseService.Application.Databases.Create;
using SqlTrainer.DatabaseService.Application.Databases.Drop;
using SqlTrainer.DatabaseService.Application.Databases.Rename;
using SqlTrainer.DatabaseService.IntegrationTests.Helpers;

namespace SqlTrainer.DatabaseService.IntegrationTests.Databases;

[Collection(DatabaseDefinition.Name)]
public class DatabaseServiceTests
{
    private const string Mssql = InitializationHelper.Mssql;
    private const string Postgres = InitializationHelper.Postgres;
    
    private DatabaseServiceTests()
    {
    }
    
    public sealed class CreateTests : DatabaseServiceTests
    {
        public static IEnumerable<object[]> GetCreateTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseServiceInitializationHelper.CreateDatabaseTestName, true]);
                testData.Add([Mssql, "Invalid Database Name", false]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseServiceInitializationHelper.CreateDatabaseTestName, true]);
                testData.Add([Postgres, "Invalid Database Name", false]);
            }
            
            return testData;
        }

        [Theory]
        [MemberData(nameof(GetCreateTestData))]
        public async Task Create_ReturnResponse(string language, string databaseName, bool shouldBeSucceed)
        {
            // Arrange
            var baseConnectionString = InitializationHelper.GetConnectionString(language);
            var request = new CreateDatabaseRequest(language, baseConnectionString, databaseName);

            // Act
            var sut = DatabaseServiceInitializationHelper.CreateCreateDatabaseRequestHandler();
            var response = await sut.HandleAsync(request);

            // Assert
            response.Should().BeOfType<Result>();
            if (shouldBeSucceed)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var scriptResult = await databaseRepository.ExecuteAsync("SELECT 1");
                scriptResult.Should().NotBeNull();
                
                InitializationHelper.DatabasesCreationNotes.AddCreationNote(language, databaseName);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeNullOrEmpty();
            }
        }
    }

    public sealed class DropTests : DatabaseServiceTests
    {
        public static IEnumerable<object[]> GetDropTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseServiceInitializationHelper.DropDatabaseTestName, true]);
                testData.Add([Mssql, "Invalid Database Name", false]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseServiceInitializationHelper.DropDatabaseTestName, true]);
                testData.Add([Postgres, "Invalid Database Name", false]);
            }
            
            return testData;
        }
        
        [Theory]
        [MemberData(nameof(GetDropTestData))]
        public async Task Drop_ReturnResponse(string language, string databaseName, bool shouldBeSucceed)
        {
            // Arrange
            var baseConnectionString = InitializationHelper.GetConnectionString(language);
            var request = new DropDatabaseRequest(language, baseConnectionString, databaseName);

            // Act
            var sut = DatabaseServiceInitializationHelper.CreateDropDatabaseRequestHandler();
            var response = await sut.HandleAsync(request);

            // Assert
            response.Should().BeOfType<Result>();
            if (shouldBeSucceed)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, databaseName);
                var act = databaseRepository.Invoking(rep => rep.ExecuteAsync("SELECT 1"));
                switch (language)
                {
                    case InitializationHelper.Mssql:
                        await act.Should().ThrowAsync<SqlException>();
                        break;
                    case InitializationHelper.Postgres:
                        await act.Should().ThrowAsync<Npgsql.PostgresException>();
                        break;
                }
                
                InitializationHelper.DatabasesCreationNotes.AddDropNote(language, databaseName);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeNullOrEmpty();
            }
        }
    }
    
    public sealed class RenameTests : DatabaseServiceTests
    {
        public static IEnumerable<object[]> GetRenameTestData()
        {
            var testData = new List<object[]>();
            
            if (InitializationHelper.ShouldRunTests(Mssql))
            {
                testData.Add([Mssql, DatabaseServiceInitializationHelper.RenameDatabaseTestName, DatabaseServiceInitializationHelper.RenameNewDatabaseTestName, true]);
                testData.Add([Mssql, "Invalid Database Name", "ValidName", false]);
                testData.Add([Mssql, "ValidName", "Invalid Database Name", false]);
                testData.Add([Mssql, "Invalid Database Name", "Invalid Database Name", false]);
            }
            
            if (InitializationHelper.ShouldRunTests(Postgres))
            {
                testData.Add([Postgres, DatabaseServiceInitializationHelper.RenameDatabaseTestName, DatabaseServiceInitializationHelper.RenameNewDatabaseTestName, true]);
                testData.Add([Postgres, "Invalid Database Name", "ValidName", false]);
                testData.Add([Postgres, "ValidName", "Invalid Database Name", false]);
                testData.Add([Postgres, "Invalid Database Name", "Invalid Database Name", false]);
            }
            
            return testData;
        }
        
        [Theory]
        [MemberData(nameof(GetRenameTestData))]
        public async Task Rename_ReturnResponse(string language, string databaseName, string newName, bool shouldBeSucceed)
        {
            // Arrange
            var baseConnectionString = InitializationHelper.GetConnectionString(language);
            var request = new RenameDatabaseRequest(language, baseConnectionString, databaseName, newName);

            // Act
            var sut = DatabaseServiceInitializationHelper.CreateRenameDatabaseRequestHandler();
            var response = await sut.HandleAsync(request);

            // Assert
            response.Should().BeOfType<Result>();
            if (shouldBeSucceed)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeNullOrEmpty();
                
                var databaseRepository = InitializationHelper.CreateDatabaseRepository(language, request.NewDatabaseName);
                var scriptResult = await databaseRepository.ExecuteAsync("SELECT 1");
                scriptResult.Should().NotBeNull();

                var oldDatabaseRepository = InitializationHelper.CreateDatabaseRepository(language, request.OldDatabaseName);
                var act = oldDatabaseRepository.Invoking(rep => rep.ExecuteAsync("SELECT 1"));
                switch (language)
                {
                    case InitializationHelper.Mssql:
                        await act.Should().ThrowAsync<SqlException>();
                        break;
                    case InitializationHelper.Postgres:
                        await act.Should().ThrowAsync<Npgsql.PostgresException>();
                        break;
                }
                
                InitializationHelper.DatabasesCreationNotes.AddDropNote(language, request.OldDatabaseName);
                InitializationHelper.DatabasesCreationNotes.AddCreationNote(language, request.NewDatabaseName);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeNullOrEmpty();
            }
        }
    }
}