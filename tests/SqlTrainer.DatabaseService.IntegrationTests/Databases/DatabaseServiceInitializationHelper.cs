using Microsoft.Extensions.Logging;
using Moq;
using SqlTrainer.DatabaseService.Application.Databases.Create;
using SqlTrainer.DatabaseService.Application.Databases.Drop;
using SqlTrainer.DatabaseService.Application.Databases.Execute;
using SqlTrainer.DatabaseService.Application.Databases.Rename;
using SqlTrainer.DatabaseService.Application.Tables.Alter;
using SqlTrainer.DatabaseService.Application.Tables.Create;
using SqlTrainer.DatabaseService.Application.Tables.DeleteData;
using SqlTrainer.DatabaseService.Application.Tables.Drop;
using SqlTrainer.DatabaseService.Application.Tables.GetData;
using SqlTrainer.DatabaseService.Application.Tables.InsertData;
using SqlTrainer.DatabaseService.Application.Tables.Rename;
using SqlTrainer.DatabaseService.Application.Tables.UpdateData;
using SqlTrainer.DatabaseService.Factories;
using SqlTrainer.DatabaseService.IntegrationTests.Helpers;

namespace SqlTrainer.DatabaseService.IntegrationTests.Databases;

public sealed class DatabaseServiceInitializationHelper : InitializationHelper
{
    public const string CreateDatabaseTestName = "CreateDatabaseTest";
    public const string DropDatabaseTestName = "DropDatabaseTest";
    
    public const string RenameDatabaseTestName = "RenameDatabaseTest";
    public const string RenameNewDatabaseTestName = "RenameNewDatabaseTest";
    
    public static async Task InitializeAsync()
    {
        await CreateDatabaseAsync(Mssql, DropDatabaseTestName);
        await CreateDatabaseAsync(Postgres, DropDatabaseTestName);
        await CreateDatabaseAsync(Mssql, RenameDatabaseTestName);
        await CreateDatabaseAsync(Postgres, RenameDatabaseTestName);
    }

    public static async Task CleanUpAsync()
    {
        await DropDatabaseAsync(Mssql, CreateDatabaseTestName);
        await DropDatabaseAsync(Postgres, CreateDatabaseTestName);
        await DropDatabaseAsync(Mssql, RenameNewDatabaseTestName);
        await DropDatabaseAsync(Postgres, RenameNewDatabaseTestName);
        try
        {
            await DropDatabaseAsync(Mssql, DropDatabaseTestName);
            await DropDatabaseAsync(Postgres, DropDatabaseTestName);
            await DropDatabaseAsync(Mssql, RenameDatabaseTestName);
            await DropDatabaseAsync(Postgres, RenameDatabaseTestName);
        }
        catch (Exception)
        {
            // We should drop databases if they have not been dropped in the tests
        }
    }
    
    public static ExecuteQueryRequestHandler CreateExecuteQueryRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var executeQueryRequestValidator = new ExecuteQueryRequestValidator();
        var executeQueryLogger = Mock.Of<ILogger<ExecuteQueryRequestHandler>>();
        var executeQueryRequestHandler = new ExecuteQueryRequestHandler(repositoryFactory, executeQueryRequestValidator, executeQueryLogger);
        return executeQueryRequestHandler;
    }
    
    public static CreateDatabaseRequestHandler CreateCreateDatabaseRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var createDatabaseRequestValidator = new CreateDatabaseRequestValidator();
        var createDatabaseLogger = Mock.Of<ILogger<CreateDatabaseRequestHandler>>();
        var createDatabaseRequestHandler = new CreateDatabaseRequestHandler(repositoryFactory, createDatabaseRequestValidator, createDatabaseLogger);
        return createDatabaseRequestHandler;
    }
    
    public static DropDatabaseRequestHandler CreateDropDatabaseRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var dropDatabaseRequestValidator = new DropDatabaseRequestValidator();
        var dropDatabaseLogger = Mock.Of<ILogger<DropDatabaseRequestHandler>>();
        var dropDatabaseRequestHandler = new DropDatabaseRequestHandler(repositoryFactory, dropDatabaseRequestValidator, dropDatabaseLogger);
        return dropDatabaseRequestHandler;
    }
    
    public static RenameDatabaseRequestHandler CreateRenameDatabaseRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var renameDatabaseRequestValidator = new RenameDatabaseRequestValidator();
        var renameDatabaseLogger = Mock.Of<ILogger<RenameDatabaseRequestHandler>>();
        var renameDatabaseRequestHandler = new RenameDatabaseRequestHandler(repositoryFactory, renameDatabaseRequestValidator, renameDatabaseLogger);
        return renameDatabaseRequestHandler;
    }
    
    public static CreateTableRequestHandler CreateCreateTableRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var createTableRequestValidator = new CreateTableRequestValidator();
        var createTableLogger = Mock.Of<ILogger<CreateTableRequestHandler>>();
        var createTableRequestHandler = new CreateTableRequestHandler(repositoryFactory, createTableRequestValidator, createTableLogger);
        return createTableRequestHandler;
    }
    
    public static DropTableRequestHandler CreateDropTableRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var dropTableRequestValidator = new DropTableRequestValidator();
        var dropTableLogger = Mock.Of<ILogger<DropTableRequestHandler>>();
        var dropTableRequestHandler = new DropTableRequestHandler(repositoryFactory, dropTableRequestValidator, dropTableLogger);
        return dropTableRequestHandler;
    }
    
    public static AlterTableRequestHandler CreateAlterTableRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var alterTableRequestValidator = new AlterTableRequestValidator();
        var alterTableLogger = Mock.Of<ILogger<AlterTableRequestHandler>>();
        var alterTableRequestHandler = new AlterTableRequestHandler(repositoryFactory, alterTableRequestValidator, alterTableLogger);
        return alterTableRequestHandler;
    }
    
    public static RenameTableRequestHandler CreateRenameTableRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var renameTableRequestValidator = new RenameTableRequestValidator();
        var renameTableLogger = Mock.Of<ILogger<RenameTableRequestHandler>>();
        var renameTableRequestHandler = new RenameTableRequestHandler(repositoryFactory, renameTableRequestValidator, renameTableLogger);
        return renameTableRequestHandler;
    }
    
    public static InsertTableDataRequestHandler CreateInsertTableDataRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var insertTableDataRequestValidator = new InsertTableDataRequestValidator();
        var insertTableDataLogger = Mock.Of<ILogger<InsertTableDataRequestHandler>>();
        var insertTableDataRequestHandler = new InsertTableDataRequestHandler(repositoryFactory, insertTableDataRequestValidator, insertTableDataLogger);
        return insertTableDataRequestHandler;
    }
    
    public static DeleteTableDataRequestHandler CreateDeleteTableDataRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var deleteTableDataRequestValidator = new DeleteTableDataRequestValidator();
        var deleteTableDataLogger = Mock.Of<ILogger<DeleteTableDataRequestHandler>>();
        var deleteTableDataRequestHandler = new DeleteTableDataRequestHandler(repositoryFactory, deleteTableDataRequestValidator, deleteTableDataLogger);
        return deleteTableDataRequestHandler;
    }
    
    public static UpdateTableDataRequestHandler CreateUpdateTableDataRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var updateTableDataRequestValidator = new UpdateTableDataRequestValidator();
        var updateTableDataLogger = Mock.Of<ILogger<UpdateTableDataRequestHandler>>();
        var updateTableDataRequestHandler = new UpdateTableDataRequestHandler(repositoryFactory, updateTableDataRequestValidator, updateTableDataLogger);
        return updateTableDataRequestHandler;
    }
    
    public static GetTableDataRequestHandler CreateGetTableDataRequestHandler()
    {
        var repositoryFactory = new RepositoryFactory();
        var selectTableDataRequestValidator = new GetTableDataRequestValidator();
        var selectTableDataLogger = Mock.Of<ILogger<GetTableDataRequestHandler>>();
        var selectTableDataRequestHandler = new GetTableDataRequestHandler(repositoryFactory, selectTableDataRequestValidator, selectTableDataLogger);
        return selectTableDataRequestHandler;
    }
}