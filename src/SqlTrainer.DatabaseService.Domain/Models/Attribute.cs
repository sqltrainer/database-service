using Domain.Models;

namespace SqlTrainer.DatabaseService.Domain.Models;

public sealed class Attribute : Model, IEquatable<Attribute>
{
    public const string UniqueConstraintSuffix = "unique";
    public const string DefaultConstraintSuffix = "default";
    
    private static readonly IReadOnlyCollection<string> QuoteTypes = new[] { "varchar", "nvarchar", "uniqueidentifier", "uuid", "datetime", "timestamp", "timestamp with time zone", "timestamp without time zone" };

    public required string Name { get; init; }
    // ToDo: Should be property 'AreQuotesRequired'
    public required string Type { get; init; }
    public int? VarcharNumberOfSymbols { get; init; }
    public required bool IsPrimaryKey { get; init; }
    public required bool IsNotNull { get; init; }
    public required bool IsUnique { get; init; }
    public string? DefaultValue { get; init; }
    public string? ForeignKeyName { get; init; }
    public string? ReferenceTableName { get; init; }
    public required string TableName { get; init; }
    public required int Order { get; init; }
    
    public bool IsVarchar => Type.Equals("varchar", StringComparison.OrdinalIgnoreCase) && VarcharNumberOfSymbols > 0;
    public bool IsForeignKey => !string.IsNullOrWhiteSpace(ForeignKeyName) &&
                                !string.IsNullOrWhiteSpace(ReferenceTableName);

    public string UniqueConstraintName => $"{TableName.ToLower()}_{Name.ToLower()}_{UniqueConstraintSuffix}";
    public string DefaultConstraintName => $"{TableName.ToLower()}_{Name.ToLower()}_{DefaultConstraintSuffix}";

    public bool NeedQuotes() => QuoteTypes.Contains(Type.ToLower());

    public override bool Equals(object? obj) => obj is Attribute attribute && this.Equals(attribute);
    public bool Equals(Attribute? other) => other is not null && other.Name.Equals(this.Name) && other.TableName.Equals(this.TableName);
    public override int GetHashCode() => HashCode.Combine(Name, TableName);
    
    public static bool operator ==(Attribute? left, Attribute? right) => left is not null && left.Equals(right);
    public static bool operator !=(Attribute? left, Attribute? right) => !(left == right);
    
    public override string ToString() => $"{Name} ({TableName})";
}