namespace SqlTrainer.DatabaseService.Domain.Models;

public abstract class AttributeValue
{
    public required string Name { get; init; }
    public required string Type { get; init; }
    public required bool IsPrimaryKey { get; init; }
    public required bool AreQuotesRequired { get; init; }
    public required string Value { get; init; }
    
    public abstract string DatabaseValue { get; }
}