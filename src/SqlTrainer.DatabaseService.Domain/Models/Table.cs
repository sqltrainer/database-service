using Domain.Models;

namespace SqlTrainer.DatabaseService.Domain.Models;

public sealed class Table : Model, IEquatable<Table>
{
    public required string Name { get; init; }
    public required IReadOnlyCollection<Attribute> Attributes { get; init; }

    public IReadOnlyCollection<Attribute> PrimaryKeys => Attributes.Where(a => a.IsPrimaryKey).ToHashSet();
    public IReadOnlyCollection<IGrouping<string, Attribute>> ForeignKeys => Attributes.Where(a => a.IsForeignKey).GroupBy(a => a.ReferenceTableName!).ToHashSet();

    public string PrimaryKeyConstraintName => $"{Name.ToLower()}_{string.Join('_', PrimaryKeys.Select(pk => pk.Name.ToLower()).Order())}_pk";

    public string GetForeignKeyConstraintName(string referenceTableName)
    {
        if (!ForeignKeys.Any())
            return string.Empty;

        var foreignKeys = ForeignKeys.FirstOrDefault(fk => fk.Key.Equals(referenceTableName));
        if (foreignKeys is null)
            return string.Empty;
        
        return $"{Name.ToLower()}_{string.Join('_', foreignKeys.Select(fk => fk.Name.ToLower()).Order())}_fk";
    }

    public override bool Equals(object? obj) => obj is Table table && this.Equals(table);
    public bool Equals(Table? other) => other is not null && other.Name.Equals(this.Name);
    public override int GetHashCode() => HashCode.Combine(Name);
    
    public static bool operator ==(Table? left, Table? right) => left is not null && left.Equals(right);
    public static bool operator !=(Table? left, Table? right) => !(left == right);
    
    public override string ToString() => Name;

}