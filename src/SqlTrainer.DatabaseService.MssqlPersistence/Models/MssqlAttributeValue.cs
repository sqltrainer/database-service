using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.MssqlPersistence.Models;

public sealed class MssqlAttributeValue : AttributeValue
{
    public override string DatabaseValue => AreQuotesRequired ? $"'{Value}'" : Value;
}
