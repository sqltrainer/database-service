using Microsoft.Data.SqlClient;
using Persistence.Contexts;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Persistence.Repositories;

namespace SqlTrainer.DatabaseService.MssqlPersistence.Repositories;

public sealed class DatabaseRepository : ScriptRepository, IDatabaseRepository
{
    public DatabaseRepository(DatabaseContext context) : base(context)
    {
    }

    public async Task CreateAsync(string name, CancellationToken cancellationToken = default)
    {
        await using var connection = new SqlConnection(Context.ConnectionString);
        await using var command = new SqlCommand($"create database [{name}]", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    public async Task RenameAsync(string name, string newName, CancellationToken cancellationToken = default)
    {
        await SetSingleUserModeAsync(name, cancellationToken);
        
        await using var connection = new SqlConnection(Context.ConnectionString);
        var commandText = $@"
alter database [{name}] set single_user with rollback immediate;
alter database [{name}] modify name = [{newName}];
alter database [{newName}] set multi_user;
";
        await using var command = new SqlCommand(commandText, connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    public async Task DropAsync(string name, CancellationToken cancellationToken = default)
    {
        await SetSingleUserModeAsync(name, cancellationToken);
        
        await using var connection = new SqlConnection(Context.ConnectionString);
        await using var command = new SqlCommand($"drop database [{name}]", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    private async Task SetSingleUserModeAsync(string name, CancellationToken cancellationToken = default)
    {
        await using var connection = new SqlConnection(Context.ConnectionString);
        await using var command = new SqlCommand($"alter database [{name}] set single_user with rollback immediate", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }
}