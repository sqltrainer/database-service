using DatabaseHelper.ScriptResults;
using Persistence.Contexts;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Domain.Models;
using SqlTrainer.DatabaseService.MssqlPersistence.Models;
using SqlTrainer.DatabaseService.Persistence.Helpers;
using SqlTrainer.DatabaseService.Persistence.Repositories;

namespace SqlTrainer.DatabaseService.MssqlPersistence.Repositories;

public sealed class TableRepository : ScriptRepository, ITableRepository
{
    private const char LeftBracket = '[';
    private const char RightBracket = ']';
    
    public TableRepository(DatabaseContext context) : base(context)
    {
    }

    public async Task CreateAsync(Table table, CancellationToken cancellationToken = default) => 
        await Context.CallAsync(table.ToCreateQuery(LeftBracket, RightBracket), cancellationToken);

    public async Task DropAsync(string name, CancellationToken cancellationToken = default) =>
        await Context.CallAsync($"drop table [{name}]", cancellationToken);

    public async Task RenameAsync(string oldName, string newName, CancellationToken cancellationToken = default) =>
        await Context.CallAsync($"exec sp_rename \'dbo.{oldName}\', \'{newName}\'", cancellationToken);

    public async Task<ScriptResult> GetDataAsync(Table table, CancellationToken cancellationToken = default) =>
        await ExecuteAsync(table.ToSelectQuery(LeftBracket, RightBracket), cancellationToken);

    public async Task InsertAsync(Table table, string jsonData, CancellationToken cancellationToken = default)
    {
        var parsed = ParseJson(jsonData, table.Attributes, CreateAttributeValue);
        var insertQueries = table.ToInsertQueries(parsed, LeftBracket, RightBracket);
        foreach (var query in insertQueries)
            await Context.CallAsync(query, cancellationToken);
    }

    public async Task UpdateAsync(Table table, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default)
    {
        var oldParsed = ParseJson(oldJsonData, table.Attributes, CreateAttributeValue).FirstOrDefault()?.Select(a => a.Value);
        var newParsed = ParseJson(newJsonData, table.Attributes, CreateAttributeValue).FirstOrDefault();

        if (oldParsed is null || newParsed is null)
            return;
        
        await Context.CallAsync(table.ToUpdateQuery(oldParsed.Where(a => a.IsPrimaryKey), newParsed, LeftBracket, RightBracket), cancellationToken);
    }

    public async Task DeleteAsync(Table table, string jsonData, CancellationToken cancellationToken = default)
    {
        var parsed = ParseJson(jsonData, table.Attributes, CreateAttributeValue).FirstOrDefault()?.Select(a => a.Value);

        if (parsed is null)
            return;
        
        await Context.CallAsync(table.ToDeleteQuery(parsed, LeftBracket, RightBracket), cancellationToken);
    }
    
    public async Task AddPrimaryKeyConstraintAsync(Table table, CancellationToken cancellationToken = default)
    {
        var query = $"alter table [{table.Name}] add constraint [{table.PrimaryKeyConstraintName}] primary key ({string.Join(", ", table.PrimaryKeys.Select(a => $"[{a.Name}]"))})";
        await Context.CallAsync(query, cancellationToken);
    }

    public async Task AddForeignKeyConstraintAsync(Table table, CancellationToken cancellationToken = default)
    {
        foreach (var foreignKeys in table.ForeignKeys)
        {
            var constraintName = table.GetForeignKeyConstraintName(foreignKeys.Key);
            var attributes = foreignKeys.Select(a => $"[{a.Name}]");
            var references = foreignKeys.Select(a => $"[{a.ForeignKeyName}]");
            
            var query = $"alter table [{table.Name}] add constraint [{constraintName}] foreign key ({string.Join(", ", attributes)}) references [{foreignKeys.Key}] ({string.Join(", ", references)})";
            await Context.CallAsync(query, cancellationToken);
        }
    }

    private static MssqlAttributeValue CreateAttributeValue(
        string name, string type, bool isPrimaryKey, bool areQuotesRequired, string value) => new()
    {
        Name = name,
        Type = type,
        IsPrimaryKey = isPrimaryKey,
        AreQuotesRequired = areQuotesRequired,
        Value = value
    };
}