using Persistence.Contexts;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Persistence.Repositories;

namespace SqlTrainer.DatabaseService.MssqlPersistence.Repositories;

using AttributeModel = Domain.Models.Attribute;

public sealed class AttributeRepository : ScriptRepository, IAttributeRepository
{
    public AttributeRepository(DatabaseContext context) : base(context)
    {
    }

    public async Task CreateAsync(AttributeModel attribute, CancellationToken cancellationToken = default)
    {
        var type = attribute.IsVarchar ? $"{attribute.Type} ({attribute.VarcharNumberOfSymbols})" : attribute.Type;
        var nullable = attribute.IsNotNull ? "not null" : string.Empty;
        var query = $"alter table [{attribute.TableName}] add [{attribute.Name}] {type} {nullable}";
        await Context.CallAsync(query, cancellationToken);
    }

    public async Task RenameAsync(string tableName, string oldName, string newName, CancellationToken cancellationToken = default)
    {
        var query = $"sp_rename 'dbo.{tableName}.{oldName}', '{newName}', 'COLUMN'";
        await Context.CallAsync(query, cancellationToken);
    }

    public Task DropAsync(string tableName, string name, CancellationToken cancellationToken = default)
    {
        var query = $"alter table [{tableName}] drop column [{name}]";
        return Context.CallAsync(query, cancellationToken);
    }

    public async Task ChangeTypeAsync(string tableName, AttributeModel attribute, CancellationToken cancellationToken = default)
    {
        var type = attribute.IsVarchar ? $"{attribute.Type} ({attribute.VarcharNumberOfSymbols})" : attribute.Type;
        var nullable = attribute.IsNotNull ? "not null" : string.Empty;
        var query = $"alter table [{tableName}] alter column [{attribute.Name}] {type} {nullable}";
        await Context.CallAsync(query, cancellationToken);
    }

    public async Task DropConstraintAsync(string tableName, string constraintName, CancellationToken cancellationToken = default)
    {
        var query = $"alter table [{tableName}] drop constraint [{constraintName}]";
        await Context.CallAsync(query, cancellationToken);
    }

    public async Task AddUniqueConstraintAsync(string tableName, string name, string constraintName, CancellationToken cancellationToken = default)
    {
        var query = $"alter table [{tableName}] add constraint [{constraintName}] unique ([{name}])";
        await Context.CallAsync(query, cancellationToken);
    }

    public async Task AddDefaultConstraintAsync(string tableName, string name, string constraintName, string defaultValue, CancellationToken cancellationToken = default)
    {
        var query = $"alter table [{tableName}] add constraint [{constraintName}] default {defaultValue} for [{name}]";
        await Context.CallAsync(query, cancellationToken);
    }
}