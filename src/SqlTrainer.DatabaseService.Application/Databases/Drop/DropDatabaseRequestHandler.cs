using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Drop;

public sealed class DropDatabaseRequestHandler : DatabaseServiceRequestHandler<DropDatabaseRequest>, IDropDatabaseRequestHandler
{
    private readonly ILogger<DropDatabaseRequestHandler> logger;
    
    public DropDatabaseRequestHandler(
        IRepositoryFactory repositoryFactory, 
        IDropDatabaseRequestValidator validator,
        ILogger<DropDatabaseRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(DropDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        var databaseRepository = RepositoryFactory.CreateDatabaseRepository(request.ConnectionString, request.Language);
        await databaseRepository.DropAsync(request.DatabaseName, cancellationToken);
    }

    protected override Task HandleErrorAsync(DropDatabaseRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Database {Name} for {Language} has not been dropped", request.DatabaseName, request.Language);
        return Task.CompletedTask;
    }
}