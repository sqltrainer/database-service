using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Drop;

public interface IDropDatabaseRequestHandler : IRequestHandler<DropDatabaseRequest>
{
}