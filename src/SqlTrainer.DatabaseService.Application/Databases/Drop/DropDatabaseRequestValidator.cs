using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Drop;

public sealed class DropDatabaseRequestValidator : RequestValidator<DropDatabaseRequest>, IDropDatabaseRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<DropDatabaseRequest> validationResult, DropDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("Connection string is required");

        if (string.IsNullOrWhiteSpace(request.DatabaseName))
            validationResult.FailValidation("Database name is required");

        if (!NameValidator.IsValid(request.DatabaseName))
            validationResult.FailValidation("Database name is invalid. Should contain only letters, numbers and underscores. Should start with a letter");
        
        return Task.CompletedTask;
    }
}