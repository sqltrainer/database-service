using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Drop;

public interface IDropDatabaseRequestValidator : IRequestValidator<DropDatabaseRequest>
{
}