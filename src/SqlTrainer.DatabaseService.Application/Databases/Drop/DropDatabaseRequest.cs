using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Databases.Drop;

public record DropDatabaseRequest(string Language, string ConnectionString, string DatabaseName) : Request(Language, ConnectionString);