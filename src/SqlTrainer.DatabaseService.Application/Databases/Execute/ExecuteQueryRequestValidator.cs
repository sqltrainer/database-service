using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Execute;

public sealed class ExecuteQueryRequestValidator : RequestValidator<ExecuteQueryRequest>, IExecuteQueryRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<ExecuteQueryRequest> validationResult, ExecuteQueryRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("Connection string is required");
        
        if (string.IsNullOrWhiteSpace(request.Query))
            validationResult.FailValidation("Query is required");
        else if (!QueryValidator.IsValid(request.Query))
            validationResult.FailValidation("Query is invalid. Check for forbidden keywords");
        
        return Task.CompletedTask;
    }
}