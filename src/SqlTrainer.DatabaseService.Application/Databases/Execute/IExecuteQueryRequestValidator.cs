using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Execute;

public interface IExecuteQueryRequestValidator : IRequestValidator<ExecuteQueryRequest>
{
}