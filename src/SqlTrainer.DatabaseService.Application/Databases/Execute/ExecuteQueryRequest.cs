using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Databases.Execute;

public sealed record ExecuteQueryRequest(
    string Language,
    string ConnectionString,
    string Query) : Request(Language, ConnectionString);