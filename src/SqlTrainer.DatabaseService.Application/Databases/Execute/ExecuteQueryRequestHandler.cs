using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Execute;

public sealed class ExecuteQueryRequestHandler(
    IRepositoryFactory repositoryFactory,
    IExecuteQueryRequestValidator validator,
    ILogger<ExecuteQueryRequestHandler> logger)
    : DatabaseServiceRequestHandler<ExecuteQueryRequest, ScriptResult>(repositoryFactory, validator),
        IExecuteQueryRequestHandler
{
    protected override bool UseUnitOfWork => false;

    protected override async Task<ScriptResult?> HandleRequestAsync(ExecuteQueryRequest request, CancellationToken cancellationToken = default)
    {
        var databaseRepository = RepositoryFactory.CreateDatabaseRepository(request.ConnectionString, request.Language);
        return await databaseRepository.ExecuteAsync(request.Query, cancellationToken);
    }

    protected override Task HandleErrorAsync(ExecuteQueryRequest request, Exception exception,CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Script has not been executed");
        return Task.CompletedTask;
    }
}