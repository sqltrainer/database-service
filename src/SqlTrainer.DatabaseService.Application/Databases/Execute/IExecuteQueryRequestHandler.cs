using Application.Mediator.Handlers;
using DatabaseHelper.ScriptResults;

namespace SqlTrainer.DatabaseService.Application.Databases.Execute;

public interface IExecuteQueryRequestHandler : IRequestHandler<ExecuteQueryRequest, ScriptResult>
{
}