using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Create;

public interface ICreateDatabaseRequestValidator : IRequestValidator<CreateDatabaseRequest>
{
}