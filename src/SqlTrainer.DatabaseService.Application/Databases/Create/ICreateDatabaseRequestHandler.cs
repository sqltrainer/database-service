using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Create;

public interface ICreateDatabaseRequestHandler : IRequestHandler<CreateDatabaseRequest>
{
}