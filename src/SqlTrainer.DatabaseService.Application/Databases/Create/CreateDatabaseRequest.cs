using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Databases.Create;

public sealed record CreateDatabaseRequest(
    string Language,
    string ConnectionString,
    string DatabaseName) 
    : Request(Language, ConnectionString);