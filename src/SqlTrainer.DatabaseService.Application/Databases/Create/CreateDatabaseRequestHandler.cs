using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Create;

public sealed class CreateDatabaseRequestHandler : DatabaseServiceRequestHandler<CreateDatabaseRequest>, ICreateDatabaseRequestHandler
{
    private readonly ILogger<CreateDatabaseRequestHandler> logger;
    
    public CreateDatabaseRequestHandler(
        IRepositoryFactory repositoryFactory, 
        ICreateDatabaseRequestValidator validator,
        ILogger<CreateDatabaseRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(CreateDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        var databaseRepository = RepositoryFactory.CreateDatabaseRepository(request.ConnectionString, request.Language);
        await databaseRepository.CreateAsync(request.DatabaseName, cancellationToken);
    }

    protected override Task HandleErrorAsync(CreateDatabaseRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Database {Name} for {Language} has not been created", request.DatabaseName, request.Language);
        return Task.CompletedTask;
    }
}