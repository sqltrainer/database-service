using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Rename;

public sealed class RenameDatabaseRequestValidator : RequestValidator<RenameDatabaseRequest>, IRenameDatabaseRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<RenameDatabaseRequest> validationResult, RenameDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("Connection string is required");
        
        if (string.IsNullOrWhiteSpace(request.OldDatabaseName))
            validationResult.FailValidation("Old database name is required");
        
        if (string.IsNullOrWhiteSpace(request.NewDatabaseName))
            validationResult.FailValidation("New database name is required");
        
        if (!NameValidator.IsValid(request.OldDatabaseName))
            validationResult.FailValidation("Old database name is invalid. Should contain only letters, numbers and underscores. Should start with a letter");
        
        if (!NameValidator.IsValid(request.NewDatabaseName))
            validationResult.FailValidation("New database name is invalid. Should contain only letters, numbers and underscores. Should start with a letter");

        return Task.CompletedTask;
    }
}