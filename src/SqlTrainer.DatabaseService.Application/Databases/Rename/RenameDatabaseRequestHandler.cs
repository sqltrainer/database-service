using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Rename;

public sealed class RenameDatabaseRequestHandler : DatabaseServiceRequestHandler<RenameDatabaseRequest>, IRenameDatabaseRequestHandler
{
    private readonly ILogger<RenameDatabaseRequestHandler> logger;

    public RenameDatabaseRequestHandler(
        IRepositoryFactory repositoryFactory, 
        IRenameDatabaseRequestValidator validator, 
        ILogger<RenameDatabaseRequestHandler> logger) 
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(RenameDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        var databaseRepository = RepositoryFactory.CreateDatabaseRepository(request.ConnectionString, request.Language);
        await databaseRepository.RenameAsync(request.OldDatabaseName, request.NewDatabaseName, cancellationToken);
    }

    protected override Task HandleErrorAsync(RenameDatabaseRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Database {Name} for {Language} has not been renamed to {NewName}", request.OldDatabaseName, request.Language, request.NewDatabaseName);
        return Task.CompletedTask;
    }
}