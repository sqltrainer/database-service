using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Databases.Rename;

public interface IRenameDatabaseRequestValidator : IRequestValidator<RenameDatabaseRequest>
{
}