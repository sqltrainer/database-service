using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Databases.Rename;

public record RenameDatabaseRequest(
    string Language,
    string ConnectionString,
    string OldDatabaseName,
    string NewDatabaseName) : Request(Language, ConnectionString);