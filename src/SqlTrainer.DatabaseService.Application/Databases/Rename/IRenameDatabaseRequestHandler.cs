using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Databases.Rename;

public interface IRenameDatabaseRequestHandler : IRequestHandler<RenameDatabaseRequest>
{
}