using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Rename;

public sealed class RenameTableRequestHandler : DatabaseServiceRequestHandler<RenameTableRequest>, IRenameTableRequestHandler
{
    private readonly ILogger<RenameTableRequestHandler> logger;
    
    public RenameTableRequestHandler(
        IRepositoryFactory repositoryFactory,
        IRenameTableRequestValidator validator,
        ILogger<RenameTableRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(RenameTableRequest request, CancellationToken cancellationToken = default)
    {
        if (request.OldName.Equals(request.NewName))
            return;
        
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);

        await tableRepository.RenameAsync(request.OldName, request.NewName, cancellationToken);    
    }

    protected override Task HandleErrorAsync(RenameTableRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Table {Table} could not be renamed to {NewTable}", request.OldName, request.NewName);
        return Task.CompletedTask;
    }
}
