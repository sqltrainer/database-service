using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Tables.Rename;

public sealed record RenameTableRequest(
    string Language,
    string ConnectionString,
    string OldName,
    string NewName) : Request(Language, ConnectionString);
