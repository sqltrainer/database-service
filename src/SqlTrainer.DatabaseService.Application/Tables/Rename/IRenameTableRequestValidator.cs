using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Rename;

public interface IRenameTableRequestValidator : IRequestValidator<RenameTableRequest>
{
}
