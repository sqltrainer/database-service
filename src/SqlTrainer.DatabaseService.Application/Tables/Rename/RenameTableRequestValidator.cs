using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Rename;

public sealed class RenameTableRequestValidator : RequestValidator<RenameTableRequest>, IRenameTableRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<RenameTableRequest> validationResult, RenameTableRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("ConnectionString is required");
        
        if (string.IsNullOrWhiteSpace(request.OldName))
            validationResult.FailValidation("Name is required");
        
        if (!NameValidator.IsValid(request.OldName))
            validationResult.FailValidation($"Name {request.OldName} is invalid");
        
        if (string.IsNullOrWhiteSpace(request.NewName))
            validationResult.FailValidation("Name is required");
        
        if (!NameValidator.IsValid(request.NewName))
            validationResult.FailValidation($"Name {request.NewName} is invalid");
        
        return Task.CompletedTask;
    }
}
