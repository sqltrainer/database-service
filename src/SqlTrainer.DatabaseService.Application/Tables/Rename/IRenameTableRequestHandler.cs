using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Rename;

public interface IRenameTableRequestHandler : IRequestHandler<RenameTableRequest>
{
}
