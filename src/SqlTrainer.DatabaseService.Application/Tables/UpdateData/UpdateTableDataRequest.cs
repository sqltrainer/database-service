using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.UpdateData;

public sealed record UpdateTableDataRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<TableAttributeRequest> Attributes,
    string OldJsonData,
    string NewJsonData) : Request(Language, ConnectionString);
