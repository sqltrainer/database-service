using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.UpdateData;

public interface IUpdateTableDataRequestHandler : IRequestHandler<UpdateTableDataRequest>
{
}
