using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.UpdateData;

public interface IUpdateTableDataRequestValidator : IRequestValidator<UpdateTableDataRequest>
{
}
