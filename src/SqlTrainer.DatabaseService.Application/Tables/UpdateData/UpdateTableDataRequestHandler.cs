using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.UpdateData;

public sealed class UpdateTableDataRequestHandler : DatabaseServiceRequestHandler<UpdateTableDataRequest>, IUpdateTableDataRequestHandler
{
    private readonly ILogger<UpdateTableDataRequestHandler> logger;
    
    public UpdateTableDataRequestHandler(
        IRepositoryFactory repositoryFactory,
        IUpdateTableDataRequestValidator validator,
        ILogger<UpdateTableDataRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(UpdateTableDataRequest request, CancellationToken cancellationToken = default)
    {
        var table = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes.Select(attribute => new Domain.Models.Attribute
            {
                Name = attribute.Name,
                Type = attribute.Type,
                IsNotNull = attribute.IsNotNull,
                IsPrimaryKey = attribute.IsPrimaryKey,
                IsUnique = attribute.IsUnique,
                DefaultValue = attribute.DefaultValue,
                ForeignKeyName = attribute.ForeignKeyName,
                ReferenceTableName = attribute.ReferenceTableName,
                Order = attribute.Order,
                TableName = request.Name
            }).ToArray()
        };
        
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        await tableRepository.UpdateAsync(table, request.OldJsonData, request.NewJsonData, cancellationToken);
    }

    protected override Task HandleErrorAsync(UpdateTableDataRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "");
        return Task.CompletedTask;
    }
}
