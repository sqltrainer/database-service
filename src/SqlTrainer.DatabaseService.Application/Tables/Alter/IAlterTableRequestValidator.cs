using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Alter;

public interface IAlterTableRequestValidator : IRequestValidator<AlterTableRequest>
{
}
