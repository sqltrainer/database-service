using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.Alter;

public sealed class AlterTableRequestValidator : RequestValidator<AlterTableRequest>, IAlterTableRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<AlterTableRequest> validationResult, AlterTableRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("ConnectionString is required");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required");
        
        if (!NameValidator.IsValid(request.Name))
            validationResult.FailValidation($"Name {request.Name} is invalid");
        
        if (!request.Attributes.Any())
            validationResult.FailValidation("Attributes are required");

        foreach (var attribute in request.Attributes)
        {
            if (attribute.Old is not null)
                ValidateAttribute(validationResult, attribute.Old);
            
            if (attribute.New is not null)
                ValidateAttribute(validationResult, attribute.New);
        }
        
        return Task.CompletedTask;
    }
    
    private static void ValidateAttribute(ValidationResult<AlterTableRequest> validationResult, TableAttributeRequest attribute)
    {
        if (string.IsNullOrWhiteSpace(attribute.Name))
            validationResult.FailValidation("Attribute name is required");
            
        if (!NameValidator.IsValid(attribute.Name))
            validationResult.FailValidation($"Attribute name {attribute.Name} is invalid");

        if (string.IsNullOrWhiteSpace(attribute.Type))
            validationResult.FailValidation($"Attribute {attribute.Name} type is required");
            
        if (attribute.VarcharNumberOfSymbols is not null && attribute.VarcharNumberOfSymbols <= 0)
            validationResult.FailValidation("Attribute varchar number of symbols is invalid");
            
        if (attribute.ForeignKeyName is not null && !NameValidator.IsValid(attribute.ForeignKeyName))
            validationResult.FailValidation("Attribute foreign key name is invalid");
            
        if (attribute.ReferenceTableName is not null && !NameValidator.IsValid(attribute.ReferenceTableName))
            validationResult.FailValidation("Attribute reference table name is invalid");
    }
}
