using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Alter;

public interface IAlterTableRequestHandler : IRequestHandler<AlterTableRequest>
{
}
