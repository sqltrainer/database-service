using System.Text.Json.Serialization;
using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.Alter;

public sealed record AlterTableRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<AlterTableAttributeVersions> Attributes) : Request(Language, ConnectionString);

public sealed record AlterTableAttributeVersions(
    [property: JsonPropertyName("old")] TableAttributeRequest? Old,
    [property: JsonPropertyName("new")] TableAttributeRequest? New);
