using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.Alter;

public sealed class AlterTableRequestHandler : DatabaseServiceRequestHandler<AlterTableRequest>, IAlterTableRequestHandler
{
    private readonly ILogger<AlterTableRequestHandler> logger;
    
    public AlterTableRequestHandler(
        IRepositoryFactory repositoryFactory,
        IAlterTableRequestValidator validator,
        ILogger<AlterTableRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(AlterTableRequest request, CancellationToken cancellationToken = default)
    {
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        var attributeRepository = RepositoryFactory.CreateAttributeRepository(request.ConnectionString, request.Language);

        var oldTable = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes
                .Where(a => a.Old is not null)
                .Select(a => ToModel(request.Name, a.Old!))
                .ToArray()
        };
        
        var newTable = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes
                .Where(a => a.New is not null)
                .Select(a => ToModel(request.Name, a.New!))
                .ToArray()
        };

        var attributesToDelete = request.Attributes.Where(v => v.New is null).Select(v => v.Old!);
        await RemoveAttributesAsync(attributeRepository, oldTable, attributesToDelete, cancellationToken);
        
        var attributesToUpdate = request.Attributes.Where(v => v.New is not null);
        await UpdateAttributesAsync(tableRepository, attributeRepository, oldTable, newTable, attributesToUpdate, cancellationToken);
    }

    private static async Task RemoveAttributesAsync(IAttributeRepository attributeRepository, Table table, IEnumerable<TableAttributeRequest> attributes, CancellationToken cancellationToken)
    {
        var attributesToDelete = table.Attributes.Where(a => attributes.Any(r => r.Name.Equals(a.Name))).ToArray();
        foreach (var attribute in attributesToDelete)
        {
            if (attribute.IsUnique)
                await attributeRepository.DropConstraintAsync(table.Name, attribute.UniqueConstraintName, cancellationToken);
            
            if (attribute.DefaultValue is not null)
                await attributeRepository.DropConstraintAsync(table.Name, attribute.DefaultConstraintName, cancellationToken);
        }

        if (attributesToDelete.Any(a => a.IsPrimaryKey))
            await attributeRepository.DropConstraintAsync(table.Name, table.PrimaryKeyConstraintName, cancellationToken);
        
        if (attributesToDelete.Any(a => !string.IsNullOrWhiteSpace(a.ForeignKeyName)))
            foreach (var referenceTableGroup in attributesToDelete.Where(a => !string.IsNullOrWhiteSpace(a.ReferenceTableName)).GroupBy(a => a.ReferenceTableName))
                await attributeRepository.DropConstraintAsync(table.Name, table.GetForeignKeyConstraintName(referenceTableGroup.Key!), cancellationToken);

        foreach (var attribute in attributesToDelete)
            await attributeRepository.DropAsync(table.Name, attribute.Name, cancellationToken);
    }

    private static async Task UpdateAttributesAsync(
        ITableRepository tableRepository, 
        IAttributeRepository attributeRepository, 
        Table oldTable,
        Table newTable, 
        IEnumerable<AlterTableAttributeVersions> attributeVersions, 
        CancellationToken cancellationToken)
    {
        var isPkChanged = false;
        var isFkChanged = false;
        
        foreach (var attribute in attributeVersions)
        {
            var oldAttribute = oldTable.Attributes.FirstOrDefault(a => a.Name.Equals(attribute.Old?.Name));
            var newAttribute = newTable.Attributes.First(a => a.Name.Equals(attribute.New!.Name));

            if (oldAttribute is null)
                await CreateAttributeAsync(attributeRepository, newTable, newAttribute, cancellationToken);
            else
                await UpdateAttributeAsync(attributeRepository, newTable, oldAttribute, newAttribute, cancellationToken);

            if (newAttribute.IsPrimaryKey && oldAttribute?.IsPrimaryKey != newAttribute.IsPrimaryKey)
                isPkChanged = true;

            if (!string.Equals(oldAttribute?.ForeignKeyName?.Trim(), newAttribute.ForeignKeyName?.Trim()) ||
                !string.Equals(oldAttribute?.ReferenceTableName?.Trim(), newAttribute.ReferenceTableName?.Trim()))
                isFkChanged = true;
        }

        if (isPkChanged)
            await UpdatePrimaryKey(tableRepository, attributeRepository, oldTable, newTable, cancellationToken);

        if (isFkChanged)
            await UpdateForeignKey(tableRepository, attributeRepository, oldTable, newTable, cancellationToken);
    }

    private static async Task CreateAttributeAsync(IAttributeRepository attributeRepository, Table table, Domain.Models.Attribute attribute, CancellationToken cancellationToken)
    {
        await attributeRepository.CreateAsync(attribute, cancellationToken);
        
        if (attribute.IsUnique)
            await attributeRepository.AddUniqueConstraintAsync(table.Name, attribute.Name, attribute.UniqueConstraintName, cancellationToken);
        
        if (attribute.DefaultValue is not null)
            await attributeRepository.AddDefaultConstraintAsync(table.Name, attribute.Name, attribute.DefaultConstraintName, attribute.DefaultValue, cancellationToken);
    }
    
    private static async Task UpdateAttributeAsync(IAttributeRepository attributeRepository, Table table, Domain.Models.Attribute oldAttribute, Domain.Models.Attribute newAttribute, CancellationToken cancellationToken)
    {
        if (!oldAttribute.Name.Equals(newAttribute.Name))
            await attributeRepository.RenameAsync(table.Name, oldAttribute.Name, newAttribute.Name, cancellationToken);
        
        var hasChangedVarcharNumberOfSymbols = oldAttribute.IsVarchar && newAttribute.IsVarchar && oldAttribute.VarcharNumberOfSymbols != newAttribute.VarcharNumberOfSymbols;
        if (!oldAttribute.Type.Equals(newAttribute.Type) || oldAttribute.IsNotNull != newAttribute.IsNotNull || hasChangedVarcharNumberOfSymbols)
            await attributeRepository.ChangeTypeAsync(table.Name, newAttribute, cancellationToken);
        
        if (oldAttribute.IsUnique && !newAttribute.IsUnique)
            await attributeRepository.DropConstraintAsync(table.Name, oldAttribute.UniqueConstraintName, cancellationToken);
        else if (!oldAttribute.IsUnique && newAttribute.IsUnique)
            await attributeRepository.AddUniqueConstraintAsync(table.Name, newAttribute.Name, newAttribute.UniqueConstraintName, cancellationToken);

        if (!string.Equals(oldAttribute.DefaultValue?.Trim(), newAttribute.DefaultValue?.Trim()))
        {
            if (oldAttribute.DefaultValue is not null)
                await attributeRepository.DropConstraintAsync(table.Name, oldAttribute.DefaultConstraintName, cancellationToken);
            
            if (newAttribute.DefaultValue is not null)
                await attributeRepository.AddDefaultConstraintAsync(table.Name, newAttribute.Name, newAttribute.DefaultConstraintName, newAttribute.DefaultValue, cancellationToken);
        }
    }

    private static async Task UpdatePrimaryKey(ITableRepository tableRepository, IAttributeRepository attributeRepository, Table oldTable, Table newTable, CancellationToken cancellationToken)
    {
        if (oldTable.PrimaryKeys.Any())
            await attributeRepository.DropConstraintAsync(newTable.Name, oldTable.PrimaryKeyConstraintName, cancellationToken);

        if (newTable.PrimaryKeys.Any())
            await tableRepository.AddPrimaryKeyConstraintAsync(newTable, cancellationToken);
    }
    
    private static async Task UpdateForeignKey(ITableRepository tableRepository, IAttributeRepository attributeRepository, Table oldTable, Table newTable, CancellationToken cancellationToken)
    {
        foreach (var foreignKeyGroup in oldTable.ForeignKeys)
            await attributeRepository.DropConstraintAsync(newTable.Name, foreignKeyGroup.Key, cancellationToken);

        await tableRepository.AddForeignKeyConstraintAsync(newTable, cancellationToken);
    }

    private static Domain.Models.Attribute ToModel(string tableName, TableAttributeRequest attribute) => new()
    {
        Name = attribute.Name,
        TableName = tableName,
        Type = attribute.Type,
        DefaultValue = attribute.DefaultValue,
        VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols,
        ForeignKeyName = attribute.ForeignKeyName,
        ReferenceTableName = attribute.ReferenceTableName,
        IsNotNull = attribute.IsNotNull,
        IsPrimaryKey = attribute.IsPrimaryKey,
        IsUnique = attribute.IsUnique,
        Order = attribute.Order
    };
    
    protected override Task HandleErrorAsync(AlterTableRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "");
        return Task.CompletedTask;
    }
}
