using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.Create;

public sealed class CreateTableRequestHandler : DatabaseServiceRequestHandler<CreateTableRequest>, ICreateTableRequestHandler
{
    private readonly ILogger<CreateTableRequestHandler> logger;
    
    public CreateTableRequestHandler(
        IRepositoryFactory repositoryFactory, 
        ICreateTableRequestValidator validator,
        ILogger<CreateTableRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(CreateTableRequest request, CancellationToken cancellationToken = default)
    {
        var table = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes.Select(a => new Domain.Models.Attribute
            {
                Name = a.Name,
                Type = a.Type,
                TableName = request.Name,
                IsNotNull = a.IsNotNull,
                IsPrimaryKey = a.IsPrimaryKey,
                IsUnique = a.IsUnique,
                DefaultValue = a.DefaultValue,
                VarcharNumberOfSymbols = a.VarcharNumberOfSymbols,
                ForeignKeyName = a.ForeignKeyName,
                ReferenceTableName = a.ReferenceTableName,
                Order = a.Order
            }).ToArray()
        };

        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        await tableRepository.CreateAsync(table, cancellationToken);
    }

    protected override Task HandleErrorAsync(CreateTableRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Table {Name} has not been created for language {Language}", request.Name, request.Language);
        return Task.CompletedTask;
    }
}