using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Create;

public interface ICreateTableRequestValidator : IRequestValidator<CreateTableRequest>
{
}