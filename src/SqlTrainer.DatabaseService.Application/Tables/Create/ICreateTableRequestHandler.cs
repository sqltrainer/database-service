using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Create;

public interface ICreateTableRequestHandler : IRequestHandler<CreateTableRequest>
{
}