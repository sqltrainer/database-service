using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.Create;

public sealed record CreateTableRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<TableAttributeRequest> Attributes) : Request(Language, ConnectionString);
