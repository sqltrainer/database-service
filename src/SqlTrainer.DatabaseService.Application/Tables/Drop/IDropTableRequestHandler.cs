using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Drop;

public interface IDropTableRequestHandler : IRequestHandler<DropTableRequest>
{
}