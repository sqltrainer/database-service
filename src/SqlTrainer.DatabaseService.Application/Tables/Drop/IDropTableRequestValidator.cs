using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Drop;

public interface IDropTableRequestValidator : IRequestValidator<DropTableRequest>
{
}
