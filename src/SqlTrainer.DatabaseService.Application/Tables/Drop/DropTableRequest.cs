using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Tables.Drop;

public sealed record DropTableRequest(
    string Language,
    string ConnectionString,
    string Name) : Request(Language, ConnectionString);
