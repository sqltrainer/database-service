
using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.Drop;

public sealed class DropTableRequestValidator : RequestValidator<DropTableRequest>, IDropTableRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<DropTableRequest> validationResult, DropTableRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("ConnectionString is required");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required");
        
        if (!NameValidator.IsValid(request.Name))
            validationResult.FailValidation($"Name {request.Name} is invalid");
        
        return Task.CompletedTask;
    }
}
