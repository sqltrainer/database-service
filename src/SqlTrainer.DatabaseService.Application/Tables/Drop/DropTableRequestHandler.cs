using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.Drop;

public sealed class DropTableRequestHandler : DatabaseServiceRequestHandler<DropTableRequest>, IDropTableRequestHandler
{
    private readonly ILogger<DropTableRequestHandler> logger;
    
    public DropTableRequestHandler(
        IRepositoryFactory repositoryFactory,
        IDropTableRequestValidator validator,
        ILogger<DropTableRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(DropTableRequest request, CancellationToken cancellationToken = default)
    {
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        await tableRepository.DropAsync(request.Name, cancellationToken);
    }

    protected override Task HandleErrorAsync(DropTableRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Table {Name} has not been dropped for language {Language}", request.Name, request.Language);
        return Task.CompletedTask;
    }
}
