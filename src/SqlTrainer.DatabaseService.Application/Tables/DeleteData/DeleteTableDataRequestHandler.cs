using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.DeleteData;

public sealed class DeleteTableDataRequestHandler : DatabaseServiceRequestHandler<DeleteTableDataRequest>, IDeleteTableDataRequestHandler
{
    private readonly ILogger<DeleteTableDataRequestHandler> logger;
    
    public DeleteTableDataRequestHandler(
        IRepositoryFactory repositoryFactory,
        IDeleteTableDataRequestValidator validator,
        ILogger<DeleteTableDataRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(DeleteTableDataRequest request, CancellationToken cancellationToken = default)
    {
        var table = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes.Select(attribute => new Domain.Models.Attribute
            {
                Name = attribute.Name,
                Type = attribute.Type,
                IsNotNull = attribute.IsNotNull,
                IsPrimaryKey = attribute.IsPrimaryKey,
                IsUnique = attribute.IsUnique,
                DefaultValue = attribute.DefaultValue,
                ForeignKeyName = attribute.ForeignKeyName,
                ReferenceTableName = attribute.ReferenceTableName,
                Order = attribute.Order,
                TableName = request.Name
            }).ToArray()
        };
        
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        await tableRepository.DeleteAsync(table, request.JsonData, cancellationToken);
    }

    protected override Task HandleErrorAsync(DeleteTableDataRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Table data have not been deleted for table {Table} for language {Language}", request.Name, request.Language);
        return Task.CompletedTask;
    }
}
