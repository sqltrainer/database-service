using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.DeleteData;

public interface IDeleteTableDataRequestValidator : IRequestValidator<DeleteTableDataRequest>
{
}
