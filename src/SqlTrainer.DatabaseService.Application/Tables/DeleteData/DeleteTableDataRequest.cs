using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.DeleteData;

public sealed record DeleteTableDataRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<TableAttributeRequest> Attributes,
    string JsonData) : Request(Language, ConnectionString);
