using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.DeleteData;

public interface IDeleteTableDataRequestHandler : IRequestHandler<DeleteTableDataRequest>
{
}
