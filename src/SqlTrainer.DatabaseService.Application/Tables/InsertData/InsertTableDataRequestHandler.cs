using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.InsertData;

public sealed class InsertTableDataRequestHandler : DatabaseServiceRequestHandler<InsertTableDataRequest>, IInsertTableDataRequestHandler
{
    private readonly ILogger<InsertTableDataRequestHandler> logger;
    
    public InsertTableDataRequestHandler(
        IRepositoryFactory repositoryFactory,
        IInsertTableDataRequestValidator validator,
        ILogger<InsertTableDataRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task HandleRequestAsync(InsertTableDataRequest request, CancellationToken cancellationToken = default)
    {
        var table = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes.Select(attribute => new Domain.Models.Attribute
            {
                Name = attribute.Name,
                Type = attribute.Type,
                IsNotNull = attribute.IsNotNull,
                IsPrimaryKey = attribute.IsPrimaryKey,
                IsUnique = attribute.IsUnique,
                DefaultValue = attribute.DefaultValue,
                ForeignKeyName = attribute.ForeignKeyName,
                ReferenceTableName = attribute.ReferenceTableName,
                Order = attribute.Order,
                TableName = request.Name
            }).ToArray()
        };
        
        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        await tableRepository.InsertAsync(table, request.JsonData, cancellationToken);
    }

    protected override Task HandleErrorAsync(InsertTableDataRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Data could not be inserted to table {Name} for language {Language}", request.Name, request.Language);
        return Task.CompletedTask;
    }
}
