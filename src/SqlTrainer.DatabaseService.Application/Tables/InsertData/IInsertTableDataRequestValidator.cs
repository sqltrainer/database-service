using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.InsertData;

public interface IInsertTableDataRequestValidator : IRequestValidator<InsertTableDataRequest>
{
}
