using Application.Mediator.Handlers;

namespace SqlTrainer.DatabaseService.Application.Tables.InsertData;

public interface IInsertTableDataRequestHandler : IRequestHandler<InsertTableDataRequest>
{
}
