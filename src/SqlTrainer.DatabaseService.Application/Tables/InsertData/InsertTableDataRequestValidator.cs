using Application.Mediator.Validators;
using SqlTrainer.DatabaseService.Application.Shared.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.InsertData;

public sealed class InsertTableDataRequestValidator : RequestValidator<InsertTableDataRequest>, IInsertTableDataRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<InsertTableDataRequest> validationResult, InsertTableDataRequest request, CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Language))
            validationResult.FailValidation("Language is required");
        
        if (string.IsNullOrWhiteSpace(request.ConnectionString))
            validationResult.FailValidation("ConnectionString is required");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required");
        
        if (!NameValidator.IsValid(request.Name))
            validationResult.FailValidation("Name is invalid");
        
        if (!request.Attributes.Any())
            validationResult.FailValidation("Attributes are required");

        foreach (var attribute in request.Attributes)
        {
            if (string.IsNullOrWhiteSpace(attribute.Name))
                validationResult.FailValidation("Attribute name is required");
            
            if (!NameValidator.IsValid(attribute.Name))
                validationResult.FailValidation("Attribute name is invalid");
            
            if (string.IsNullOrWhiteSpace(attribute.Type))
                validationResult.FailValidation("Attribute type is required");
        }
        
        if (string.IsNullOrWhiteSpace(request.JsonData))
            validationResult.FailValidation("JsonData is required");

        if (request.JsonData.Contains('\\') && QueryValidator.ForbiddenWords.Any(word => request.JsonData.Contains($" {word} ")))
            validationResult.FailValidation("JsonData is invalid");
        
        return Task.CompletedTask;
    }
}
