using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.InsertData;

public sealed record InsertTableDataRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<TableAttributeRequest> Attributes,
    string JsonData) : Request(Language, ConnectionString);
