using Application.Mediator.Handlers;
using DatabaseHelper.ScriptResults;

namespace SqlTrainer.DatabaseService.Application.Tables.GetData;

public interface IGetTableDataRequestHandler : IRequestHandler<GetTableDataRequest, ScriptResult>
{
}
