using SqlTrainer.DatabaseService.Application.Shared.Requests;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Application.Tables.GetData;

public sealed record GetTableDataRequest(
    string Language,
    string ConnectionString,
    string Name,
    IEnumerable<TableAttributeRequest> Attributes) : Request(Language, ConnectionString);

