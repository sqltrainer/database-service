using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Handlers;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Tables.GetData;

public sealed class GetTableDataRequestHandler : DatabaseServiceRequestHandler<GetTableDataRequest, ScriptResult>, IGetTableDataRequestHandler
{
    private readonly ILogger<GetTableDataRequestHandler> logger;

    protected override bool UseUnitOfWork => false;

    public GetTableDataRequestHandler(
        IRepositoryFactory repositoryFactory,
        IGetTableDataRequestValidator validator,
        ILogger<GetTableDataRequestHandler> logger)
        : base(repositoryFactory, validator)
    {
        this.logger = logger;
    }
    
    protected override async Task<ScriptResult?> HandleRequestAsync(GetTableDataRequest request, CancellationToken cancellationToken = default)
    {
        var table = new Table
        {
            Name = request.Name,
            Attributes = request.Attributes.Select(a => new Domain.Models.Attribute
            {
                Name = a.Name,
                Type = a.Type,
                IsNotNull = a.IsNotNull,
                IsUnique = a.IsUnique,
                IsPrimaryKey = a.IsPrimaryKey,
                DefaultValue = a.DefaultValue,
                VarcharNumberOfSymbols = a.VarcharNumberOfSymbols,
                ForeignKeyName = a.ForeignKeyName,
                ReferenceTableName = a.ReferenceTableName,
                Order = a.Order,
                TableName = request.Name
            }).ToArray()
        };

        var tableRepository = RepositoryFactory.CreateTableRepository(request.ConnectionString, request.Language);
        return await tableRepository.GetDataAsync(table, cancellationToken);
    }

    protected override Task HandleErrorAsync(GetTableDataRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Script {Script} has not been executed for language {Language}", request.Name, request.Language);
        return Task.CompletedTask;
    }
}
