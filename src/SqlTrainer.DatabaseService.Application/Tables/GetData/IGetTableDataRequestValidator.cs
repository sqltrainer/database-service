using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.GetData;

public interface IGetTableDataRequestValidator : IRequestValidator<GetTableDataRequest>
{
}
