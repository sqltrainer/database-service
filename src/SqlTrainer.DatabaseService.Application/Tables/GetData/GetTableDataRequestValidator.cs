using Application.Mediator.Validators;

namespace SqlTrainer.DatabaseService.Application.Tables.GetData;

public sealed class GetTableDataRequestValidator : RequestValidator<GetTableDataRequest>, IGetTableDataRequestValidator
{
    protected override Task ValidateAsync(ValidationResult<GetTableDataRequest> validationResult, GetTableDataRequest dataRequest, CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }
}
