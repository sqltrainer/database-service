﻿using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

public record TableAttributeRequest(
    [property: JsonPropertyName("name")] string Name,
    [property: JsonPropertyName("type")] string Type,
    [property: JsonPropertyName("default")] string? DefaultValue,
    [property: JsonPropertyName("length")] int? VarcharNumberOfSymbols,
    [property: JsonPropertyName("primaryKey")] bool IsPrimaryKey,
    [property: JsonPropertyName("notnull")] bool IsNotNull,
    [property: JsonPropertyName("unique")] bool IsUnique,
    [property: JsonPropertyName("foreignKey")] string? ForeignKeyName,
    [property: JsonPropertyName("reference")] string? ReferenceTableName,
    [property: JsonPropertyName("order")] int Order);