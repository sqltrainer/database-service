using Application.Repositories;
using SqlTrainer.DatabaseService.Application.Repositories;

namespace SqlTrainer.DatabaseService.Application.Factories;

public interface IRepositoryFactory
{
    IUnitOfWork CreateUnitOfWork(string connectionString, string language);
    IDatabaseRepository CreateDatabaseRepository(string connectionString, string language);
    ITableRepository CreateTableRepository(string connectionString, string language);
    IAttributeRepository CreateAttributeRepository(string connectionString, string language);
}