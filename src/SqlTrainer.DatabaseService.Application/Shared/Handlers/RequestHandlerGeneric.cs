using Application.Mediator.Handlers;
using Application.Mediator.Validators;
using Results;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Shared.Handlers;

public abstract class DatabaseServiceRequestHandler<TRequest, TResponse>(
    IRepositoryFactory repositoryFactory,
    IRequestValidator<TRequest> validator)
    : RequestHandler<TRequest, TResponse>(validator)
    where TRequest : Request
{
    protected readonly IRepositoryFactory RepositoryFactory = repositoryFactory;
    
    protected virtual bool UseUnitOfWork => true;

    public override async Task<Result<TResponse>> HandleAsync(TRequest request, CancellationToken cancellationToken = default)
    {
        var unitOfWork = UseUnitOfWork ? RepositoryFactory.CreateUnitOfWork(request.ConnectionString, request.Language) : null;
        var result = Result<TResponse>.Create();
        
        try
        {
            var validationResult = await Validator!.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid)
            {
                result.Fail(validationResult.Errors.Select(e => e.Message));
                return result;
            }
            
            var response = await HandleRequestAsync(request, cancellationToken);
            unitOfWork?.SaveChanges();
            
            if (response is not null)
                result.Complete(response);
            else
                result.SetNotFound();
        }
        catch (Exception ex)
        {
            unitOfWork?.Cancel();
            result.Fail(ex.Message);
            await HandleErrorAsync(request, ex, cancellationToken);
        }

        return result;
    }
}