using Application.Mediator.Handlers;
using Application.Mediator.Validators;
using Results;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Shared.Requests;

namespace SqlTrainer.DatabaseService.Application.Shared.Handlers;

public abstract class DatabaseServiceRequestHandler<TRequest>(
    IRepositoryFactory repositoryFactory,
    IRequestValidator<TRequest> validator)
    : RequestHandler<TRequest>(validator)
    where TRequest : Request
{
    protected readonly IRepositoryFactory RepositoryFactory = repositoryFactory;
    
    protected virtual bool UseUnitOfWork => true;

    public override async Task<Result> HandleAsync(TRequest request, CancellationToken cancellationToken = default)
    {
        var unitOfWork = UseUnitOfWork ? RepositoryFactory.CreateUnitOfWork(request.ConnectionString, request.Language) : null;
        var result = Result.Create();
        
        try
        {
            var validationResult = await Validator!.ValidateAsync(request, cancellationToken);
            if (!validationResult.IsValid)
            {
                result.Fail(validationResult.Errors.Select(e => e.Message));
                return result;
            }
            
            await HandleRequestAsync(request, cancellationToken);
            unitOfWork?.SaveChanges();
            result.Complete();
        }
        catch (Exception ex)
        {
            unitOfWork?.Cancel();
            result.Fail(ex.Message);
            await HandleErrorAsync(request, ex, cancellationToken);
        }

        return result;
    }
}