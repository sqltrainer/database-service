using Application.Mediator.Requests;

namespace SqlTrainer.DatabaseService.Application.Shared.Requests;

public abstract record Request(string Language, string ConnectionString) : IRequest;