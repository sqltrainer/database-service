using System.Text.RegularExpressions;

namespace SqlTrainer.DatabaseService.Application.Shared.Validators;

public static class NameValidator
{
    private const string NameRegex = @"^[a-zA-Z][a-zA-Z0-9_]*$";
    
    public static bool IsValid(string name)
    {
        return !string.IsNullOrWhiteSpace(name) &&
               !name.Contains(' ') &&
               Regex.IsMatch(name, NameRegex, RegexOptions.IgnoreCase);
    }
}