namespace SqlTrainer.DatabaseService.Application.Shared.Validators;

public static class QueryValidator
{
    public static readonly string[] ForbiddenWords = { "delete", "drop", "update", "insert", "create", "alter", "truncate" };
    
    public static bool IsValid(string query)
    {
        if (string.IsNullOrWhiteSpace(query))
            return false;
        
        var cleanedQuery = query.Trim();
        while (cleanedQuery.Contains('\''))
        {
            var left = cleanedQuery.IndexOf('\'');
            var right = cleanedQuery.IndexOf('\'', left + 1);
            cleanedQuery = right == -1 ? cleanedQuery.Remove(left) : cleanedQuery.Remove(left, right - left + 1);
        }

        return !string.IsNullOrWhiteSpace(cleanedQuery) && 
               ForbiddenWords.All(word => !cleanedQuery.Contains($" {word} "));
    }
}