﻿using System.Reflection;
using Application.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SqlTrainer.DatabaseService.Application.Extensions;

public static class DependencyInjectionExtensions
{
    public static void AddApplication(this IServiceCollection services, ILogger? logger = null)
    {
        var assembly = Assembly.GetExecutingAssembly();
        services.InjectRequestStuff(assembly, logger);
    }
}