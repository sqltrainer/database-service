namespace SqlTrainer.DatabaseService.Application.Repositories;

public interface IAttributeRepository
{
    Task CreateAsync(Domain.Models.Attribute attribute, CancellationToken cancellationToken = default);
    Task RenameAsync(string tableName, string oldName, string newName, CancellationToken cancellationToken = default);
    Task DropAsync(string tableName, string name, CancellationToken cancellationToken = default);
    Task ChangeTypeAsync(string tableName, Domain.Models.Attribute attribute, CancellationToken cancellationToken = default);
    Task DropConstraintAsync(string tableName, string constraintName, CancellationToken cancellationToken = default);
    Task AddUniqueConstraintAsync(string tableName, string name, string constraintName, CancellationToken cancellationToken = default);
    Task AddDefaultConstraintAsync(string tableName, string name, string constraintName, string defaultValue, CancellationToken cancellationToken = default);
}