using DatabaseHelper.ScriptResults;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Application.Repositories;

public interface ITableRepository
{
    Task CreateAsync(Table table, CancellationToken cancellationToken = default);
    Task DropAsync(string name, CancellationToken cancellationToken = default);
    Task RenameAsync(string oldName, string newName, CancellationToken cancellationToken = default);
    Task<ScriptResult> GetDataAsync(Table table, CancellationToken cancellationToken = default);
    Task InsertAsync(Table table, string jsonData, CancellationToken cancellationToken = default);
    Task UpdateAsync(Table table, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default);
    Task DeleteAsync(Table table, string jsonData, CancellationToken cancellationToken = default);
    Task AddPrimaryKeyConstraintAsync(Table table, CancellationToken cancellationToken = default);
    Task AddForeignKeyConstraintAsync(Table table, CancellationToken cancellationToken = default);
}