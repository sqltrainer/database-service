using DatabaseHelper.ScriptResults;

namespace SqlTrainer.DatabaseService.Application.Repositories;

public interface IDatabaseRepository
{
    Task CreateAsync(string name, CancellationToken cancellationToken = default);
    Task RenameAsync(string name, string newName, CancellationToken cancellationToken = default);
    Task DropAsync(string name, CancellationToken cancellationToken = default);
    Task<ScriptResult> ExecuteAsync(string script, CancellationToken cancellationToken = default);
}