using Persistence.Contexts;
using Persistence.Repositories;
using DatabaseHelper.ScriptResults;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Persistence.Repositories;

using AttributeModel = Domain.Models.Attribute;

public abstract class ScriptRepository : Repository
{
    protected ScriptRepository(DatabaseContext context) : base(context)
    {
    }
    
    public async Task<ScriptResult> ExecuteAsync(string script, CancellationToken cancellationToken = default)
    {
        using var reader = await Context.ExecuteReaderAsync(script, cancellationToken);
        var scriptResult = new ScriptResult { Script = script };
        while (reader.Read())
            scriptResult.Add(reader);
        scriptResult.Lock();
        return scriptResult;
    }

    protected static IReadOnlyCollection<IDictionary<string, TAttributeValue>> ParseJson<TAttributeValue>(
        string data, IReadOnlyCollection<AttributeModel> attributes,
        Func<string, string, bool, bool, string, TAttributeValue> createAttributeValue)
        where TAttributeValue : AttributeValue
    {
        var dataObjects = new List<string>();
        while (data.IndexOf('{') != -1)
        {
            var leftFigureBracket = data.IndexOf('{');
            var rightFigureBracket = data.IndexOf('}');
            var leftStartIndex = leftFigureBracket + 1;
            var length = rightFigureBracket - leftFigureBracket - 1;
            dataObjects.Add(data.Substring(leftStartIndex, length));
            data = data.Remove(leftStartIndex - 1, length + 2);
        }
        
        var objectAttributeValues = new List<IDictionary<string, TAttributeValue>>();
        foreach (var obj in dataObjects)
        {
            var attributeValues = new Dictionary<string, TAttributeValue>();
            foreach (var attribute in obj.Split(','))
            {
                var doubleDotIndex = attribute.IndexOf(':');
                var name = attribute[..doubleDotIndex].Replace("\"", string.Empty);
                var value = attribute[(doubleDotIndex + 1)..];
                
                var attr = attributes.First(a => a.Name.Equals(name));
                var areQuotesRequired = attr.NeedQuotes();
                value = value.Replace("\"", string.Empty);

                if (attr.IsNotNull is false && string.IsNullOrWhiteSpace(value.Replace("\'", string.Empty)))
                    continue;

                attributeValues.Add(name, createAttributeValue(name, attr.Type, attr.IsPrimaryKey, areQuotesRequired, value));
            }
            objectAttributeValues.Add(attributeValues);
        }

        return objectAttributeValues;
    }
}