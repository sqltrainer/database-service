using System.Text;
using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.Persistence.Helpers;

public static class QueryHelper
{
    public static string ToCreateQuery(this Table table, char leftQuote, char rightQuote) => new StringBuilder()
        .Append("create table ").Append(leftQuote).Append(table.Name).Append(rightQuote).Append('(')
        .Append(string.Join(", ", table.Attributes.Select(a => BuildAttribute(a, leftQuote, rightQuote))))
        .Append(BuildPrimaryKey(table, leftQuote, rightQuote))
        .Append(BuildForeignKeys(table, leftQuote, rightQuote))
        .Append(')')
        .ToString();

    public static string ToSelectQuery(this Table table, char leftQuote, char rightQuote) => new StringBuilder()
        .Append("select ")
        .Append(string.Join(", ", table.Attributes.OrderBy(a => a.Order).Select(a => $"{leftQuote}{a.Name}{rightQuote}")))
        .Append(" from ").Append(leftQuote).Append(table.Name).Append(rightQuote)
        .Append(" order by ")
        .Append(string.Join(", ", table.PrimaryKeys.OrderBy(a => a.Order).Select(a => $"{leftQuote}{a.Name}{rightQuote}")))
        .ToString();

    public static IEnumerable<string> ToInsertQueries<TAttributeValue>(this Table table, IEnumerable<IDictionary<string, TAttributeValue>> objectAttributeValues, char leftQuote, char rightQuote) where TAttributeValue : AttributeValue => objectAttributeValues
        .Select(attributeValues => attributeValues.OrderBy(av => av.Key).ToHashSet())
        .Select(attributeValues => new StringBuilder()
            .Append("insert into ").Append(leftQuote).Append(table.Name).Append(rightQuote).Append('(')
            .Append(string.Join(", ", attributeValues.Select(av => $"{leftQuote}{av.Key}{rightQuote}")))
            .Append(") values (")
            .Append(string.Join(", ", attributeValues.Select(a => a.Value.DatabaseValue)))
            .Append(')')
            .ToString());
    
    public static string ToUpdateQuery<TAttributeValue>(this Table table, IEnumerable<TAttributeValue> oldPrimaryKeys, IDictionary<string, TAttributeValue> attributeValues, char leftQuote, char rightQuote) where TAttributeValue : AttributeValue => new StringBuilder()
        .Append("update ").Append(leftQuote).Append(table.Name).Append(rightQuote).Append(" set ")
        .Append(string.Join(", ", attributeValues.Select(av => $"{leftQuote}{av.Key}{rightQuote} = {av.Value.DatabaseValue}")))
        .Append(" where ")
        .Append(string.Join(" and ", oldPrimaryKeys.Select(pk => $"{leftQuote}{pk.Name}{rightQuote} = {pk.DatabaseValue}")))
        .ToString();

    public static string ToDeleteQuery<TAttributeValue>(this Table table, IEnumerable<TAttributeValue> attributes, char leftQuote, char rightQuote) where TAttributeValue : AttributeValue => new StringBuilder()
        .Append("delete from ").Append(leftQuote).Append(table.Name).Append(rightQuote)
        .Append(" where ")
        .Append(string.Join(" and ", attributes.Where(a => a.IsPrimaryKey).Select(a => $"{leftQuote}{a.Name}{rightQuote} = {a.DatabaseValue}")))
        .ToString();

    private static string BuildAttribute(Domain.Models.Attribute attribute, char leftQuote, char rightQuote)
    {
        var attributeBuilder = new StringBuilder();

        attributeBuilder.Append(leftQuote).Append(attribute.Name).Append(rightQuote).Append(' ');
        attributeBuilder.Append(attribute.Type).Append(' ');
        
        if (attribute.IsVarchar)
            attributeBuilder.Append('(').Append(attribute.VarcharNumberOfSymbols).Append(") ");

        if (attribute.IsPrimaryKey || attribute.IsNotNull)
            attributeBuilder.Append("not null ");
        
        if (attribute.IsUnique)
            attributeBuilder.Append("constraint ").Append(leftQuote).Append(attribute.UniqueConstraintName).Append(rightQuote).Append(" unique ");

        if (attribute.DefaultValue is not null)
            attributeBuilder.Append("constraint ").Append(leftQuote).Append(attribute.DefaultConstraintName).Append(rightQuote).Append(" default (").Append(attribute.DefaultValue).Append(") ");

        return attributeBuilder.ToString();
    }

    private static string BuildPrimaryKey(Table table, char leftQuote, char rightQuote) =>
        $", constraint {leftQuote}{table.PrimaryKeyConstraintName}{rightQuote} primary key ({string.Join(',', table.PrimaryKeys.Select(pk => $"{leftQuote}{pk.Name}{rightQuote}"))})";

    private static string BuildForeignKeys(Table table, char leftQuote, char rightQuote)
    {
        if (!table.ForeignKeys.Any())
            return string.Empty;
        
        var foreignKeysBuilder = new StringBuilder();
        foreach (var referenceTableForeignKeys in table.ForeignKeys)
        {
            foreignKeysBuilder.Append(", constraint ")
                .Append(leftQuote).Append(table.GetForeignKeyConstraintName(referenceTableForeignKeys.Key)).Append(rightQuote)
                .Append(" foreign key (").Append(string.Join(',', referenceTableForeignKeys.Select(fk => $"{leftQuote}{fk.Name}{rightQuote}"))).Append(") references ")
                .Append(leftQuote).Append(referenceTableForeignKeys.Key).Append(rightQuote)
                .Append('(').Append(string.Join(',', referenceTableForeignKeys.Select(fk => $"{leftQuote}{fk.ForeignKeyName}{rightQuote}"))).Append(')');
        }

        return foreignKeysBuilder.ToString();
    }
}