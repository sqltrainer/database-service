using SqlTrainer.DatabaseService.Domain.Models;

namespace SqlTrainer.DatabaseService.PostgresPersistence.Models;

public sealed class PostgresAttributeValue : AttributeValue
{
    public override string DatabaseValue => AreQuotesRequired ? $"'{Value}'" : Value;
}
