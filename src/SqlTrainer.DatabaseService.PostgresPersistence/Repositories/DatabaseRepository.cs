using Npgsql;
using Persistence.Contexts;
using SqlTrainer.DatabaseService.Application.Repositories;
using SqlTrainer.DatabaseService.Persistence.Repositories;

namespace SqlTrainer.DatabaseService.PostgresPersistence.Repositories;

public sealed class DatabaseRepository : ScriptRepository, IDatabaseRepository
{
    public DatabaseRepository(DatabaseContext context) : base(context)
    {
    }

    public async Task CreateAsync(string name, CancellationToken cancellationToken = default)
    {
        await using var connection = new NpgsqlConnection(Context.ConnectionString);
        await using var command = new NpgsqlCommand($"create database \"{name}\"", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    public async Task RenameAsync(string name, string newName, CancellationToken cancellationToken = default)
    {
        await KillDatabaseConnectionsAsync(name, cancellationToken);

        await using var connection = new NpgsqlConnection(Context.ConnectionString);
        await using var command = new NpgsqlCommand($"alter database \"{name}\" rename to \"{newName}\"", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    public async Task DropAsync(string name, CancellationToken cancellationToken = default)
    {
        await KillDatabaseConnectionsAsync(name, cancellationToken);

        await using var connection = new NpgsqlConnection(Context.ConnectionString);
        await using var command = new NpgsqlCommand($"drop database \"{name}\"", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }

    private async Task KillDatabaseConnectionsAsync(string name, CancellationToken cancellationToken = default)
    {
        await using var connection = new NpgsqlConnection(Context.ConnectionString);
        await using var command = new NpgsqlCommand($"select pg_terminate_backend(pid) from pg_stat_activity where datname = '{name}'", connection);
        await connection.OpenAsync(cancellationToken);
        await command.ExecuteNonQueryAsync(cancellationToken);
    }
}