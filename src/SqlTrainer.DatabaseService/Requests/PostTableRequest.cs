using System.Text.Json.Serialization;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostTableRequest : DatabaseRequest
{
    [JsonPropertyName("name")]
    public string TableName { get; set; } = null!;

    [JsonPropertyName("attributes")]
    public IReadOnlyCollection<TableAttributeRequest> AttributeRequests { get; set; } = null!;
}