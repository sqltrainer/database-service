using System.Text.Json.Serialization;
using SqlTrainer.DatabaseService.Application.Tables.SharedRequests;

namespace SqlTrainer.DatabaseService.Requests;

public class DataRequest : DatabaseRequest
{
    [JsonPropertyName("table")]
    public string TableName { get; set; } = null!;
    
    [JsonPropertyName("attributes")]
    public IReadOnlyCollection<TableAttributeRequest> AttributeRequests { get; set; } = null!;
}