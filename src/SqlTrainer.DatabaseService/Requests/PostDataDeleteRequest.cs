using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostDataDeleteRequest : DataRequest
{
    [JsonPropertyName("data")]
    public string JsonData { get; set; } = null!;
}