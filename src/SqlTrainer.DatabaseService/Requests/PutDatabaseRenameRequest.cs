using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PutDatabaseRenameRequest : DatabaseRequest
{
    [JsonPropertyName("oldName")]
    public string OldName { get; set; } = null!;
    
    [JsonPropertyName("newName")]
    public string NewName { get; set; } = null!;
}