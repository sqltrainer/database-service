using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostDataUpdateRequest : DataRequest
{
    [JsonPropertyName("oldData")]
    public string OldJsonData { get; set; } = null!;
    
    [JsonPropertyName("newData")]
    public string NewJsonData { get; set; } = null!;
}