using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PutTableRenameRequest : DatabaseRequest
{
    [JsonPropertyName("oldName")]
    public string OldTableName { get; set; } = null!;
    
    [JsonPropertyName("newName")]
    public string NewTableName { get; set; } = null!;
}