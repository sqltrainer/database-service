using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public class PostDatabaseRequest : DatabaseRequest
{
    [JsonPropertyName("name")]
    public string DatabaseName { get; set; } = null!;
}