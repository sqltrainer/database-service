using System.Text.Json.Serialization;
using System.Web;

namespace SqlTrainer.DatabaseService.Requests;

public class DatabaseRequest
{
    // ToDo: ConnectionString should be encrypted before sending request and decrypted on the server side.
    /// <summary>
    /// The connection string to the database to connect to.
    /// </summary>
    [JsonPropertyName("connectionString")]
    public string ConnectionString { get; set; } = null!;

    /// <summary>
    /// The SQL language that is used for database requests.
    /// </summary>
    [JsonPropertyName("language")]
    public string Language { get; set; } = null!;
    
    // ReSharper disable once UnusedMember.Global
    public static bool TryParse(string query, IFormatProvider _, out DatabaseRequest? request)
    {
        request = null;
        
        var queryDictionary = HttpUtility.ParseQueryString(query);
        if (queryDictionary.Count == 0)
            return false;
        
        const string connectionStringKey = "connectionString";
        const string languageKey = "language";

        request = new DatabaseRequest
        {
            ConnectionString = queryDictionary[connectionStringKey] 
                               ?? throw new ArgumentNullException(connectionStringKey),
            Language = queryDictionary[languageKey] 
                       ?? throw new ArgumentOutOfRangeException(languageKey)
        };

        return true;
    }
}