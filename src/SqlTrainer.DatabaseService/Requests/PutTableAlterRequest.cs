using System.Text.Json.Serialization;
using SqlTrainer.DatabaseService.Application.Tables.Alter;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PutTableAlterRequest : DatabaseRequest
{
    [JsonPropertyName("name")]
    public string TableName { get; set; } = null!;

    [JsonPropertyName("attributes")]
    public IReadOnlyCollection<AlterTableAttributeVersions> AttributeVersions { get; set; } = null!;
}