using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostScriptBatchRequest : DatabaseRequest
{
    [JsonPropertyName("scripts")]
    public IReadOnlyCollection<string> Scripts { get; set; } = null!;
}