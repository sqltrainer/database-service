using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostDataInsertRequest : DataRequest
{
    [JsonPropertyName("data")]
    public string JsonData { get; set; } = null!;
}