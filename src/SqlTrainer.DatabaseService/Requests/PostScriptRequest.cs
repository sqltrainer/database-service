using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseService.Requests;

public sealed class PostScriptRequest : DatabaseRequest
{
    [JsonPropertyName("script")]
    public string Script { get; set; } = null!;
}