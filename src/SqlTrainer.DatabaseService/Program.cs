using SqlTrainer.DatabaseService.Application.Extensions;
using SqlTrainer.DatabaseService.Factories;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Endpoints;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddScoped<IRepositoryFactory, RepositoryFactory>()
    .AddApplication();

var app = builder.Build();

app.MapDatabaseEndpoints();
app.MapTableEndpoints();
app.MapDataEndpoints();
app.MapScriptEndpoints();

app.Run();