using DatabaseHelper.ScriptResults;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseService.Application.Tables.DeleteData;
using SqlTrainer.DatabaseService.Application.Tables.GetData;
using SqlTrainer.DatabaseService.Application.Tables.InsertData;
using SqlTrainer.DatabaseService.Application.Tables.UpdateData;
using SqlTrainer.DatabaseService.Requests;

namespace SqlTrainer.DatabaseService.Endpoints;

public static class DataEndpoints
{
    public static void MapDataEndpoints(this IEndpointRouteBuilder routesBuilder)
    {
        var group = routesBuilder.MapGroup("data");
        
        group.MapPost("insert", InsertDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("insertData")
            .WithDisplayName("Insert Data to Table");
        
        group.MapPost("delete", DeleteDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("deleteData")
            .WithDisplayName("Delete Data from Table");
        
        group.MapPost("update", UpdateDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("updateData")
            .WithDisplayName("Update Data in Table");
        
        group.MapGet("select", SelectDataAsync)
            .Produces<ScriptResult>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .WithName("selectData")
            .WithDisplayName("Select Data from Table");
    }
    
    private static async Task<IResult> InsertDataAsync(
        [FromServices] IInsertTableDataRequestHandler handler,
        [FromBody] PostDataInsertRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new InsertTableDataRequest(
            request.Language, request.ConnectionString, request.TableName, request.AttributeRequests, request.JsonData);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> DeleteDataAsync(
        [FromServices] IDeleteTableDataRequestHandler handler,
        [FromBody] PostDataDeleteRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteTableDataRequest(
            request.Language, request.ConnectionString, request.TableName, request.AttributeRequests, request.JsonData);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> UpdateDataAsync(
        [FromServices] IUpdateTableDataRequestHandler handler,
        [FromBody] PostDataUpdateRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new UpdateTableDataRequest(
            request.Language, request.ConnectionString, request.TableName, request.AttributeRequests, request.OldJsonData, request.NewJsonData);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> SelectDataAsync(
        [FromServices] IGetTableDataRequestHandler handler,
        [FromQuery] DataRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetTableDataRequest(
            request.Language, request.ConnectionString, request.TableName, request.AttributeRequests);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result!.ToAspNetResult();
    }
}