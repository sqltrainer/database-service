using DatabaseHelper.ScriptResults;
using Microsoft.AspNetCore.Mvc;
using Results;
using Results.WebExtensions;
using SqlTrainer.DatabaseService.Application.Databases.Execute;
using SqlTrainer.DatabaseService.Requests;

namespace SqlTrainer.DatabaseService.Endpoints;

public static class ScriptEndpoints
{
    public static void MapScriptEndpoints(this IEndpointRouteBuilder routeBuilder)
    {
        var group = routeBuilder.MapGroup("scripts");
        
        group.MapPost("/", ExecuteScriptAsync)
            .Produces<ScriptResult>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .WithName("executeScript")
            .WithDisplayName("Execute Script");
        
        group.MapGet("/batch", ExecuteScriptsAsync)
            .Produces<IReadOnlyCollection<ScriptResult>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("executeScripts")
            .WithDisplayName("Execute Batch of Scripts");
    }
    
    private static async Task<IResult> ExecuteScriptAsync(
        [FromServices] IExecuteQueryRequestHandler handler,
        [FromBody] PostScriptRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new ExecuteQueryRequest(request.Language, request.ConnectionString, request.Script);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result!.ToAspNetResult();
    }
    
    private static async Task<IResult> ExecuteScriptsAsync(
        [FromServices] IExecuteQueryRequestHandler handler,
        [FromBody] PostScriptBatchRequest request,
        CancellationToken cancellationToken)
    {
        var results = new List<Result<ScriptResult>>();
        
        foreach (var script in request.Scripts)
        {
            var mediatorRequest = new ExecuteQueryRequest(request.Language, request.ConnectionString, script);
            
            var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

            if (result.IsFailed)
            {
                var errors = result.Errors.Select(e => $"{e}. For script\n{script}");
                result = Result<ScriptResult>.Failed(errors);
            }
            
            results.Add(result);
        }

        var batchResult = results.Any(r => r.IsSuccess)
            ? Result<IReadOnlyCollection<Result<ScriptResult>>>.Success(results)
            : Result<IReadOnlyCollection<Result<ScriptResult>>>.Failed(results.SelectMany(r => r.Errors).ToList());
        
        return batchResult!.ToAspNetResult();
    }
}