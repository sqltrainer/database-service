using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseService.Application.Tables.Alter;
using SqlTrainer.DatabaseService.Application.Tables.Create;
using SqlTrainer.DatabaseService.Application.Tables.Drop;
using SqlTrainer.DatabaseService.Application.Tables.Rename;
using SqlTrainer.DatabaseService.Requests;

namespace SqlTrainer.DatabaseService.Endpoints;

public static class TableEndpoints
{
    public static void MapTableEndpoints(this IEndpointRouteBuilder routesBuilder)
    {
        var group = routesBuilder.MapGroup("tables");
        
        group.MapPost("/", CreateTableAsync)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("createTable")
            .WithDisplayName("Create Table");
        
        group.MapDelete("/{name}", DropTableAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("dropTable")
            .WithDisplayName("Drop Table");
        
        group.MapPut("/rename", RenameTableAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("renameTable")
            .WithDisplayName("Rename Table");
        
        group.MapPut("/", AlterTableAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("alterTable")
            .WithDisplayName("Alter Table Attributes");
    }

    private static async Task<IResult> CreateTableAsync(
        [FromServices] ICreateTableRequestHandler handler,
        [FromBody] PostTableRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new CreateTableRequest(
            request.Language, request.ConnectionString, request.TableName, request.AttributeRequests);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.IsSuccess
            ? Microsoft.AspNetCore.Http.Results.Created()
            : result.ToAspNetResult();
    }

    private static async Task<IResult> DropTableAsync(
        [FromServices] IDropTableRequestHandler handler,
        [FromRoute] string name,
        [FromQuery] DatabaseRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DropTableRequest(request.Language, request.ConnectionString, name);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }

    private static async Task<IResult> RenameTableAsync(
        [FromServices] IRenameTableRequestHandler handler,
        [FromBody] PutTableRenameRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new RenameTableRequest(request.Language, request.ConnectionString, request.OldTableName, request.NewTableName);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }

    private static async Task<IResult> AlterTableAsync(
        [FromServices] IAlterTableRequestHandler handler,
        [FromBody] PutTableAlterRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new AlterTableRequest(request.Language, request.ConnectionString, request.TableName, request.AttributeVersions);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

        return result.ToAspNetResult();
    }
}