using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseService.Application.Databases.Create;
using SqlTrainer.DatabaseService.Application.Databases.Drop;
using SqlTrainer.DatabaseService.Application.Databases.Execute;
using SqlTrainer.DatabaseService.Application.Databases.Rename;
using SqlTrainer.DatabaseService.Requests;

namespace SqlTrainer.DatabaseService.Endpoints;

public static class DatabaseEndpoints
{
    public static void MapDatabaseEndpoints(this IEndpointRouteBuilder routesBuilder)
    {
        var group = routesBuilder.MapGroup("databases");

        group.MapGet("/check-connection", CheckDatabaseConnectionAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("checkDatabaseConnection")
            .WithDisplayName("Check Database Connection");

        group.MapPost("/", CreateDatabaseAsync)
            .Produces(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("createDatabase")
            .WithDisplayName("Create Database");
        
        group.MapDelete("/{name}", DropDatabaseAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("dropDatabase")
            .WithDisplayName("Drop Database");
        
        group.MapPut("/rename", RenameDatabaseAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("renameDatabase")
            .WithDisplayName("Rename Database");
    }

    private static async Task<IResult> CheckDatabaseConnectionAsync(
        [FromServices] IExecuteQueryRequestHandler handler,
        [FromQuery] DatabaseRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new ExecuteQueryRequest(request.Language, request.ConnectionString, "SELECT 1;");
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.IsSuccess
            ? Microsoft.AspNetCore.Http.Results.NoContent()
            : result!.ToAspNetResult();
    }
    
    private static async Task<IResult> CreateDatabaseAsync(
        [FromServices] ICreateDatabaseRequestHandler handler,
        [FromBody] PostDatabaseRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new CreateDatabaseRequest(request.Language, request.ConnectionString, request.DatabaseName);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.IsSuccess 
            ? Microsoft.AspNetCore.Http.Results.Created()
            : result.ToAspNetResult();
    }

    private static async Task<IResult> DropDatabaseAsync(
        [FromServices] IDropDatabaseRequestHandler handler,
        [FromRoute] string name,
        [FromQuery] DatabaseRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DropDatabaseRequest(request.Language, request.ConnectionString, name);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> RenameDatabaseAsync(
        [FromServices] IRenameDatabaseRequestHandler handler,
        [FromBody] PutDatabaseRenameRequest renameRequest,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new RenameDatabaseRequest(renameRequest.Language, renameRequest.ConnectionString, renameRequest.OldName, renameRequest.NewName);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
}