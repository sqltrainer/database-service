using Application.Repositories;
using MssqlPersistence.Contexts;
using Persistence.Configurations;
using Persistence.Contexts;
using Persistence.Repositories;
using PostgresPersistence.Contexts;
using SqlTrainer.DatabaseService.Application.Factories;
using SqlTrainer.DatabaseService.Application.Repositories;

namespace SqlTrainer.DatabaseService.Factories;

public sealed class RepositoryFactory : IRepositoryFactory
{
    private const string Mssql = "MSSQL";
    private const string Postgresql = "POSTGRESQL";
    
    private DatabaseContext? context;

    public IUnitOfWork CreateUnitOfWork(string connectionString, string language)
    {
        var databaseContext = GetOrCreateContext(connectionString, language);
        return new UnitOfWork(databaseContext);
    }
    
    public IDatabaseRepository CreateDatabaseRepository(string connectionString, string language)
    {
        var databaseContext = GetOrCreateContext(connectionString, language);
        return language.ToUpperInvariant() switch
        {
            Postgresql => new PostgresPersistence.Repositories.DatabaseRepository(databaseContext),
            Mssql => new MssqlPersistence.Repositories.DatabaseRepository(databaseContext),
            _ => throw CreateArgumentException(language)
        };
    }

    public ITableRepository CreateTableRepository(string connectionString, string language)
    {
        var databaseContext = GetOrCreateContext(connectionString, language);
        return language.ToUpperInvariant() switch
        {
            Postgresql => new PostgresPersistence.Repositories.TableRepository(databaseContext),
            Mssql => new MssqlPersistence.Repositories.TableRepository(databaseContext),
            _ => throw CreateArgumentException(language)
        };
    }

    public IAttributeRepository CreateAttributeRepository(string connectionString, string language)
    {
        var databaseContext = GetOrCreateContext(connectionString, language);
        return language.ToUpperInvariant() switch
        {
            Postgresql => new PostgresPersistence.Repositories.AttributeRepository(databaseContext),
            Mssql => new MssqlPersistence.Repositories.AttributeRepository(databaseContext),
            _ => throw CreateArgumentException(language)
        };
    }
    
    private DatabaseContext GetOrCreateContext(string connectionString, string language)
    {
        var databaseConfiguration = new DatabaseConfiguration(connectionString);
        return context ??= language.ToUpperInvariant() switch
        {
            Postgresql => new PostgresContext(databaseConfiguration),
            Mssql => new MssqlContext(databaseConfiguration),
            _ => throw CreateArgumentException(language)
        };
    }

    private static ArgumentOutOfRangeException CreateArgumentException(string language) =>
        new(nameof(language), $"Language {language} is not supported yet");
}